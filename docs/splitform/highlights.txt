New continuous formulations of systems of conservation laws are introduced and shown to be entropy conservative and entropy stable. 

Discretely entropy conservative and entropy stable schemes are extended to high order discontinuous Galerkin methods with dense mass matrices and over-integrated quadrature rules. 

Numerical experiments confirm the high order accuracy and entropy conservation/stability of the proposed methods when using dense mass matrices and over-integrated quadrature rules. 