A=zeros(8,8);
Atest=[ALPHA1*M 0 M 0;
   -C11U 0 -ALPHA1*M 0;
   -C13U 0 -ALPHA3*M 0;
   0 -C55U 0 0;
]
Btest=[RHOF/CONST1 m1/CONST1 0 0;
        0  0  0 m3/CONST3;
       -RHO/CONST1 -RHOF/CONST1 0 0;
        0 0 0 -RHOF/CONST3;
]


A(1:4,5:8)=Atest;
A(5:8, 1:4)=Btest;




Ctest=[0 ALPHA3*M 0 M;
   0 -C13U 0 -ALPHA1*M;
   0 -C33U 0 -ALPHA3*M;
   -C55U 0 0 0;
]
Dtest=[0 0 0 m1/CONST1 ;
        RHOF/CONST3  0  m3/CONST3 0;
       0 0 0  -RHOF/CONST1 ;
       -RHO/CONST3 0 -RHOF/CONST3 0;
]


B(1:4,5:8)=Ctest;
B(5:8, 1:4)=Dtest;
