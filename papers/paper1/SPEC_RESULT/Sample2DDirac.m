function srcterm=Sample2DDirac(xs,ys)
Globals2D;
srcterm=zeros(Np,1);
V2D=Vandermonde2D(Np,r,s);
[ktria,GX,GY]=sample(xs,ys);
xtemp=zeros(3,1);
ytemp=zeros(3,1);
for n=1:1:3;
    xtemp(n)=GX(ktria,n);
    ytemp(n)=GY(ktria,n);
end
xtemp(2)=xtemp(2)-xtemp(1);
xtemp(3)=xtemp(3)-xtemp(1);
ytemp(2)=ytemp(2)-ytemp(1);
ytemp(3)=ytemp(3)-ytemp(1);
iJ=1/(xtemp(2)*ytemp(3)-ytemp(2)*xtemp(3));
xo=xs-xtemp(1);
yo=ys-ytemp(1);
rk=2*(ytemp(3)*xo -xtemp(3)*yo)*iJ-1 ;
sk=2*(-ytemp(2)*xo + xtemp(2)*yo)*iJ-1 ;
Vb=Vandermonde2D(Np,rk,sk);
Jfact=J(1,ktria);
for i=1:1:Np
    for j=1:1:Np
        srcterm(i,1)=srcterm(i,1)+V2D(i,j)*Vb(1,j);
    end
end

srcterm=srcterm/Jfact;

return