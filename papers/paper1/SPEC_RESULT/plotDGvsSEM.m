clear all;
clc;
close all;

%%%%%% . SPECFEM PART%%%%%%

zsem=load('zsem_data.dat');

zsem=zsem(:,2)./(max(abs(zsem(:,2))));

dt=0.0001;
%t=(1:1:2500)*dt;
%t=t-dt;
source=load('plot_source_time_function.txt');
t=source(:,1);
t=t-t(1);
%%%%%% . DFGEM PART%%%%%%
%xdg=load('vxdg_data.dat');
zdg = load('vzdg_data.mat');
zdg=zdg.vzdatas;
p=1;
zdg=zdg(p,:);

zdg=zdg./(max(abs(zdg)));


dtdg= 2.3752e-05;%44.1942*10^-6;
Ntsteps=5714;
tdg=(1:1:Ntsteps)*dtdg;
tdg=tdg-dt;
figure('Renderer', 'painters', 'Position', [10 10 900 500]);
%plot(zdg)
%zsem = movingslope(zsem,200,2);
h1=plot(t, zsem, 'o', 'LineWidth', 1.0);
hold on;
%h2=plot(tdg, -zdg, 'o','LineWidth', 1.0);
% plot(source(:,1)-source(1,1), zsem, '-k','LineWidth', 1.0);
% 
% figure;
 %plot(tdg, zdg, '-b', 'LineWidth', 1.0);

%ylim([-5,5]);
t=source(:,1)-source(1,1);
f0=30; tR=1.20/f0;
rick = @(t) (1 - 2*(pi*f0*(t-tR)).^2).*exp(-(pi*f0*(t-tR)).^2);
sou2=rick(t);
%plot(t, -source(:,2)/(max(abs(source(:,2)))),'-k', t, sou2/(max(abs(sou2))), '-o')


return


xllf=load('bxdataLLF.mat');
y=x.bx;
ylf=xllf.bx;
%zrec=1100;
%xrec=900;
dt=1.7331e-04;
t=[1:1:1731]*dt;
y=y(4,:)./max(abs(y(4,:)));
ylf=ylf(4,:)./max(abs(ylf(4,:)));





figure('Renderer', 'painters', 'Position', [10 10 900 500]);
h1=plot(t, y, '-k' , 'LineWidth', 1.0); hold on;
h2=plot(t(1:8:end),ylf(1:8:end),  'o','MarkerEdgeColor','k','MarkerFaceColor','w');
h3=plot(t,10*( y-ylf),'--k', 'Linewidth', 2.0);
ylim([-1.2,1.2]);
xlim([0, 0.35]);
xlabel('Time (s)', 'FontSize', 24);
ylabel('Normalized {\it b_x}','Interpreter','tex','FontSize', 24);
ax = gca;
ax.FontSize = 24;
legend([h1,h2,h3], 'Lax-Freidrich', 'Local Lax-Freidrich', '10 x Resduals ');
max(y-ylf)
%%%%

x=load('bxdata.mat');
xllf=load('bzdataLLF.mat');
y=x.bz;
ylf=xllf.bz;
%zrec=1100;
%xrec=900;
dt=1.7331e-04;
t=[1:1:1731]*dt;
y=y(4,:)./max(abs(y(4,:)));
ylf=ylf(4,:)./max(abs(ylf(4,:)));
figure('Renderer', 'painters', 'Position', [10 10 900 500]);
h1=plot(t, y, '-k' , 'LineWidth', 1.0); hold on;
h2=plot(t(1:8:end),ylf(1:8:end),  'o','MarkerEdgeColor','k','MarkerFaceColor','w');
h3=plot(t,10*( y-ylf),'--k', 'Linewidth', 2.0);
ylim([-1.2,1.2]);
xlim([0, 0.35]);
xlabel('Time (s)', 'FontSize', 24);
ylabel('Normalized {\it b_z}','Interpreter','tex','FontSize', 24);
ax = gca;
ax.FontSize = 24;
legend([h1,h2,h3], 'Lax-Freidrich', 'Local Lax-Freidrich', '10 x Resduals ');
