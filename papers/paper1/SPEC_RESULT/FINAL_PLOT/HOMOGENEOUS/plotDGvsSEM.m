clear all;
clc;
close all;

%%%%%% . SPECFEM PART%%%%%%

zsem=load('zsem_dat.dat');

zsem=zsem(:,2)./(max(abs(zsem(:,2))));

source=load('plot_source_time_function.txt');
t=source(:,1);
t=t-t(1);

%%%%%% . SPECFEM PART%%%%%%



zdg = load('vzdata.mat');
zdg=zdg.vxdata;
p=3;
zdg=zdg(p,:);

zdg=zdg./(max(abs(zdg)));

dtdg= 2.4013e-05;
Ntsteps=10411;
tdg=(1:1:Ntsteps)*dtdg;
tdg=tdg-dtdg;




%%%%%%%%%%%%%%%%%%PS DATA
zps=load('vzdataPS.mat');
zps=zps.vzdata;
zps=zps./max(abs(zps));
dtps= 2e-04;
Nps=length(zps);
tps=(1:1:Nps)*dtps;






err=interp1(t,zsem,tdg);
err=err-zdg;
RMSE=rms(err(10:end-10));


errp=interp1(tps,zps,tdg);
errps=errp-zdg;
RMSEPS=rms(errps(10:end-15));

%%%%% PLOT DATA
figure('Renderer', 'painters', 'Position', [10 10 900 600]);
h1=plot(t(1:8:end), zsem(1:8:end), 'o','MarkerEdgeColor','k','MarkerFaceColor','w');
hold on;
h2=plot(tdg,zdg,'-k','LineWidth',1.5);
h3=plot(tdg,err, '--b', 'LineWidth', 1.2);

%plot([0.058, 0.058], [-0.25, 0.25],'-k', 'LineWidth', 1.2); hold on;

%plot([0.155, 0.155], [-0.25, 0.25],'--b', 'LineWidth', 1.2)




ylim([-1.2,1.2]);
xlim([0, 0.26]);

xlabel('Time (s)', 'FontSize', 24);
ylabel('Normalized vertical velocity {(\it v_3 )}','Interpreter','tex','FontSize', 24);
ax = gca;
ax.FontSize = 24;
legend([h2,h1,h3], 'WADG method','SPECFEM','Resduals ');

text(0.18,-0.75, 'RMSE=8.68e-03', 'FontSize', 22);
text(0.078, 0.5, 'P_f', 'FontSize', 24);
text(0.185, 0.4, 'P_s', 'FontSize', 24);


figure('Renderer', 'painters', 'Position', [10 10 900 600]);
h4=plot(tps(1:4:end), zps(1:4:end), 'o','MarkerEdgeColor','k','MarkerFaceColor','w');
hold on;
h5=plot(tdg,zdg,'-k','LineWidth',1.5);
h6=plot(tdg,errps,'--b','LineWidth',1.1);
ylim([-1.2,1.2]);
xlim([0, 0.26]);

xlabel('Time (s)', 'FontSize', 24);
ylabel('Normalized vertical velocity {(\it v_3 )}','Interpreter','tex','FontSize', 24);
ax = gca;
ax.FontSize = 24;
legend([h5,h4,h6], 'WADG method','Pseudo-Spectral','Resduals ');

text(0.18,-0.75, 'RMSE=3.18e-02', 'FontSize', 22);
text(0.078, 0.5, 'P_f', 'FontSize', 24);
text(0.185, 0.4, 'P_s', 'FontSize', 24);

