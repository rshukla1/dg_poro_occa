function [ktria,GX,GY]=sample(xs,ys)
Globals2D;

GX=zeros(K,3);
GY=zeros(K,3);

for k=1:1:K
    GX(k,1)=VX(EToV(k,1));
    GX(k,2)=VX(EToV(k,2));
    GX(k,3)=VX(EToV(k,3));
    GY(k,1)=VY(EToV(k,1));
    GY(k,2)=VY(EToV(k,2));
    GY(k,3)=VY(EToV(k,3));
end
    
    

for k=1:1:K
    flag=1;
    for n=1:1:3
        crossproduct=(GX(k,n)-xs)*(GY(k,mod(n,3)+1)-ys)-(GX(k,mod(n,3)+1)-xs)*(GY(k,n)-ys);
        if (crossproduct < 0);
            flag=0;
            break;
        end
    end
    if(flag==1)
        ktria=k;
    end
end
   return