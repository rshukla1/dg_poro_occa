function rhs = RHS2D(U,time)

% function [rhsu, rhsv, rhsp] = acousticsRHS2D(u,v,p)
% Purpose  : Evaluate RHS flux in 2D acoustics TM form

Globals2D;

% Define field differences at faces
Nfld = length(U);
dU = cell(Nfld,1);
Ux = cell(Nfld,1);
Uy = cell(Nfld,1);
for fld = 1:Nfld
    dU{fld} = zeros(Nfp*Nfaces,K); dU{fld}(:) = U{fld}(vmapP)-U{fld}(vmapM);
    Ur = Dr*U{fld}; Us = Ds*U{fld};    
    Ux{fld} = rx.*Ur + sx.*Us + .5*LIFT*(Fscale.*dU{fld}.*nx);
    Uy{fld} = ry.*Ur + sy.*Us + .5*LIFT*(Fscale.*dU{fld}.*ny);    
end

idx = [7 5 1 6 2 4 1 1];
signx = [1 -1 0 -1 -1 -1 1 0];
idy = [8 1 6 5 4 3 1 1];
signy = [1 0 -1 -1 -1 -1 0 1];

% penalty fluxes: An^2 * [[Q]] = An* (An*[[Q]])
ff{1} = nx.*dU{7} + ny.*dU{8};
ff{2} = -nx.*dU{5};
ff{3} = -ny.*dU{6};
ff{4} = -(ny.*dU{5} + nx.*dU{6});
ff{5} = -(nx.*dU{2} + ny.*dU{4});
ff{6} = -(ny.*dU{3} + nx.*dU{4});
ff{7} = nx.*dU{1};
ff{8} = ny.*dU{1};

% apply An again to An.*[[Q]]
f{1} = nx.*ff{7} + ny.*ff{8};
f{2} = -nx.*ff{5};
f{3} = -ny.*ff{6};
f{4} = -(ny.*ff{5} + nx.*ff{6});
f{5} = -(nx.*ff{2} + ny.*ff{4});
f{6} = -(ny.*ff{3} + nx.*ff{4});
f{7} = nx.*ff{1};
f{8} = ny.*ff{1};

tau = 1.0; % choose more carefully!

global Vq Pq
rr = cell(Nfld,1);
for fld = 1:Nfld    
    rhsx = signx(fld)*Ux{idx(fld)};
    rhsy = signy(fld)*Uy{idy(fld)};
    rr{fld} = rhsx + rhsy - tau*.5*LIFT*(Fscale.*f{fld});
    %size(rr{1})
    rr{fld} = Vq*rr{fld};
end

global invA0
global rick ptsrc ;
rhs = cell(Nfld,1);
for ii = 1:Nfld
    rhs{ii} = zeros(Np,K);   
    for jj = 1:Nfld
        rhs{ii} = rhs{ii} - Pq*(invA0{ii,jj}.*rr{jj}); % negative sign
        %rhs{ii} = rhs{ii} -   (invA0{ii,jj}.*rr{jj}); % negative sign

    end
end
rhs{3}=rhs{3} + rick(time)*ptsrc;
rhs{2}=rhs{2} + rick(time)*ptsrc;
rhs{1}=rhs{1} - rick(time)*ptsrc;
end

