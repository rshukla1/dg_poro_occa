% function Biot2D
 
clear all -globals
Globals2D
PoroelasticGlobals2D;
PoroelasticGlobals2DS;
N=3;
K1D=312;
c_flag = 0;
FinalTime = 1.2*10^6;
CFL =0.9;
 
[Nv, VX, VY, K, EToV] = unif_tri_mesh(K1D);
StartUp2D;
 
%BuildPeriodicMaps2D(100000,100000);
 
% plotting nodes
[rp sp] = EquiNodes2D(50); [rp sp] = xytors(rp,sp);
Vp = Vandermonde2D(N,rp,sp)/V;
xp = Vp*x; yp = Vp*y;
global xq Vq Pq;

Nq = 2*N+1;
[rq sq wq] = Cubature2D(Nq); % integrate u*v*c
Vq = Vandermonde2D(N,rq,sq)/V;
Pq = V*V'*Vq'*diag(wq); % J's cancel out
Mref = inv(V*V');
xq = Vq*x; yq = Vq*y;
Jq = Vq*J;

 
%%%%% params setup
 
U = cell(8,1);
for i = 1:8
    U{i} = zeros(Np,K);
end


global rick ptsrc ;
f0 = 15*(10^-6);
tR = 1.20/f0;


rick = @(t) (1 - 2*(pi*f0*(t-tR)).^2).*exp(-(pi*f0*(t-tR)).^2);



global invA0
[invA0]=q_matrix();


%%%%%%
 
time = 0;
 
% Runge-Kutta residual storage
res = cell(8,1);
for fld = 1:8
    res{fld} = zeros(Np,K);
end
 
% compute time step size
CN = (N+1)^2/2; % guessing...
CNh = max(CN*max(Fscale(:)));
dt = CFL*2/CNh;
xsrc=160000;
zsrc=290000;
[ktria, GX,GY]=sample(xsrc,zsrc);
srcterm=Sample2DDirac(zsrc,zsrc);
%global ktria srcterm 
ptsrc=zeros(Np,K);
ptsrc(:,ktria)=1;
ptsrc=(V*V')*ptsrc;
[ptsrcx, ptsrcy]=Grad2D(ptsrc);



xrec1=200000;
zrec1=293400;

xrec2=200000;
zrec2=186700;

[krecv1, GX1, GY1]=sample(xrec1,zrec1);
[krecv2, GX2, GY2]=sample(xrec2,zrec2);
tstep=0;
Ntsteps =ceil(FinalTime/dt)

bzdatar1=zeros(Np,Ntsteps);
bxdatar1=zeros(Np,Ntsteps);
vzdatar1=zeros(Np,Ntsteps);
vxdatar1=zeros(Np,Ntsteps);
vzdatasr1=zeros(Np,Ntsteps);
vxdatasr1=zeros(Np,Ntsteps);

bzdatar2=zeros(Np,Ntsteps);
bxdatar2=zeros(Np,Ntsteps);
vzdatar2=zeros(Np,Ntsteps);
vxdatar2=zeros(Np,Ntsteps);
vzdatasr2=zeros(Np,Ntsteps);
vxdatasr2=zeros(Np,Ntsteps);



while (time<FinalTime)
    tstep=tstep+1
    if(time+dt>FinalTime), dt = FinalTime-time; end

    for INTRK = 1:5
        
        timelocal = time + rk4c(INTRK)*dt;
        
        rhs = RHS2D(U,time);
        
        % initiate and increment Runge-Kutta residuals
        for fld = 1:length(U)
            res{fld} = rk4a(INTRK)*res{fld} + dt*rhs{fld};
            U{fld} = U{fld} + rk4b(INTRK) * res{fld};
        end                
        
    end




   % Increment time
   time = time+dt; 

   bx=U{5} + (RHOF/RHO)*U{7};
   bz=U{6} + (RHOF/RHO)*U{8};
   
   bzdatar1(:,tstep)=bz(:, krecv1);
   bxdatar1(:,tstep)=bx(:, krecv1);
   vxdatar1(:,tstep)=U{5}(:, krecv1);
   vzdatar1(:,tstep)=U{6}(:, krecv1);
   vxdatasr1(:,tstep)=U{7}(:, krecv1);
   vzdatasr1(:,tstep)=U{8}(:, krecv1);
   
   bzdatar2(:,tstep)=bz(:, krecv2);
   bxdatar2(:,tstep)=bx(:, krecv2);
   vxdatar2(:,tstep)=U{5}(:, krecv2);
   vzdatar2(:,tstep)=U{6}(:, krecv2);
   vxdatasr2(:,tstep)=U{7}(:, krecv2);
   vzdatasr2(:,tstep)=U{8}(:, krecv2);
   
   if(mod(tstep,6000)==0)
       WriteVTK2D(strcat(strcat('vz', num2str(tstep)), '.vtk'), N, {'vz'}, U{6});
   end


   if(mod(tstep,500)==0);
       fprintf('At time step %d the max value of v1 is %5.6f\n', tstep, max(max(U{5})));
   end
   save('vzdatar1.mat', 'vzdatar1');
   save('bzdatar1.mat', 'bzdatar1');
   save('vzdatasr1.mat','vzdatasr1');
   
   save('vxdatar1.mat', 'vxdatar1');
   save('bxdatar1.mat', 'bxdatar1');
   save('vxdatasr1.mat','vxdatasr1');
   
   save('vzdatar2.mat', 'vzdatar2');
   save('bzdatar2.mat', 'bzdatar2');
   save('vzdatasr2.mat','vzdatasr2');
   
   save('vxdatar2.mat', 'vxdatar2');
   save('bxdatar2.mat', 'bxdatar2');
   save('vxdatasr2.mat','vxdatasr2');
   




end
 
 
