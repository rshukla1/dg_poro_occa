function [beta11x,beta11z, beta12x, beta12z, beta21x, beta21z, beta22x, beta22z, eta,...
    kappa1, kappa3, c11u, c13u, c33u, c55u, alpha1,alpha3, M] = PoroelasticMaterial2D();

Globals2D;
PoroelasticGlobals2D;
PoroelasticGlobals2DS;
beta11x=zeros(Np,K); 
beta11z=zeros(Np,K); 
beta12x=zeros(Np,K); 
beta12z=zeros(Np,K); 
beta21x=zeros(Np,K); 
beta21z=zeros(Np,K);
beta22x=zeros(Np,K); 
beta22z=zeros(Np,K);
eta=zeros(Np,K);
kappa1=zeros(Np,K);
kappa3=zeros(Np,K);
c11u=zeros(Np,K);
c13u=zeros(Np,K);
c33u=zeros(Np,K);
c55u=zeros(Np,K);
alpha1=zeros(Np,K);
alpha3=zeros(Np,K);
M=zeros(Np,K);




%%% Assignment of Property
vmapTop = [];
vmapBottom = [];
index = zeros(Np,K);
index(:) = 1:Np*K;

for k=1:K
  va = EToV(k,1);
  vb = EToV(k,2);
  vc = EToV(k,3);
 

  zave = (1/3)*(VY(va) + VY(vb) + VY(vc));

  if zave > 240000
    beta11x(:,k)=BETA11X; 
    beta11z(:,k)=BETA11Z; 
    beta12x(:,k)=BETA12X; 
    beta12z(:,k)=BETA12Z; 
    beta21x(:,k)=BETA21X; 
    beta21z(:,k)=BETA21Z;
    beta22x(:,k)=BETA22X; 
    beta22z(:,k)=BETA22Z;
    eta(:,k)=ETA;
    kappa1(:,k)=KAPPA1;
    kappa3(:,k)=KAPPA3;
    c11u(:,k)=C11U;
    c13u(:,k)=C13U;
    c33u(:,k)=C33U;
    c55u(:,k)=C55U;
    alpha1(:,k)=ALPHA1;
    alpha3(:,k)=ALPHA3;
    M(:,k)=MC;
    tag(:,k) = 0;
    vmapBottom = [vmapBottom index(:,k)];
  else
    beta11x(:,k)=BETA11XS; 
    beta11z(:,k)=BETA11ZS; 
    beta12x(:,k)=BETA12XS; 
    beta12z(:,k)=BETA12ZS; 
    beta21x(:,k)=BETA21XS; 
    beta21z(:,k)=BETA21ZS;
    beta22x(:,k)=BETA22XS; 
    beta22z(:,k)=BETA22ZS;
    eta(:,k)=ETAS;
    kappa1(:,k)=KAPPA1S;
    kappa3(:,k)=KAPPA3S;
    c11u(:,k)=C11US;
    c13u(:,k)=C13US;
    c33u(:,k)=C33US;
    c55u(:,k)=C55US;
    alpha1(:,k)=ALPHA1S;
    alpha3(:,k)=ALPHA3S;
    M(:,k)=MCS;
    vmapTop = [vmapTop index(:,k)];
    tag(:,k) = 1;
  end
end


