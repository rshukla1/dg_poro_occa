function [Q]=q_matrix()
Q=zeros(8,8);
Globals2D;
PoroelasticGlobals2D;
PoroelasticGlobals2DS;
global xq;

Q=cell(8,8);

for i=1:1:8
    for j=1:1:8
        Q{i,j}=zeros(size(xq));
    end
end

vmapTop = [];
vmapBottom = [];
index = zeros(Np,K);
index(:) = 1:Np*K;
Qt0=zeros(8,8);
Qt1=zeros(8,8);

for k=1:K
  va = EToV(k,1);
  vb = EToV(k,2);
  vc = EToV(k,3);
 

  zave = (1/3)*(VY(va) + VY(vb) + VY(vc));
  if zave > 240000
     Qt0(1,1)=(1/MC) + ((ALPHA1^2)*C33 + C11*ALPHA3^2 - 2*ALPHA1*ALPHA3*C13)/(C11*C33 - C13*C13);
     Qt0(2,1)=(ALPHA1.*C33-ALPHA3*C13)/(C11*C33-C13*C13);
     Qt0(3,1)=(ALPHA3*C11-ALPHA1*C13)/(C11*C33-C13*C13);

     Qt0(1,2)=(ALPHA1*C33-ALPHA3*C13)/(C11*C33-(C13)*(C13));
     Qt0(2,2)=C33/(C11*C33-C13*C13);
     Qt0(3,2)=-C13/(C11*C33-(C13)*(C13));

     Qt0(1,3)=(ALPHA3*C11-ALPHA1*C13)/(C11*C33 - C13*C13);
     Qt0(2,3)=-(C13)/(C11*C33 - C13*C13);
     Qt0(3,3)=(C11)/(C11*C33 - C13*C13);
     Qt0(4,4)=1/C55;


      Qt0(5,5)=RHO;
      Qt0(5,7)=RHOF;
      Qt0(6,6)=RHO;
      Qt0(6,8)=RHOF;
      Qt0(7,5)=RHOF;
      Qt0(7,7)=m1;
      Qt0(8,6)=RHOF;
      Qt0(8,8)=m3;
      Qti0=inv(Qt0);
      
      Q{1,1}(:,k)=Qti0(1,1);
      Q{2,1}(:,k)=Qti0(2,1);
      Q{3,1}(:,k)=Qti0(3,1);
      Q{1,2}(:,k)=Qti0(1,2);
      Q{2,2}(:,k)=Qti0(2,2);
      Q{3,2}(:,k)=Qti0(3,2);
      Q{1,3}(:,k)=Qti0(1,3);
      Q{2,3}(:,k)=Qti0(2,3);
      Q{3,3}(:,k)=Qti0(3,3);
      Q{4,4}(:,k)=Qti0(4,4);
      Q{5,5}(:,k)=Qti0(5,5);
      Q{5,7}(:,k)=Qti0(5,7);
      Q{6,6}(:,k)=Qti0(6,6);
      Q{6,8}(:,k)=Qti0(6,8);
      Q{7,5}(:,k)=Qti0(7,5);
      Q{7,7}(:,k)=Qti0(7,7);
      Q{8,6}(:,k)=Qti0(8,6);
      Q{8,8}(:,k)=Qti0(8,8);
  else
      Qt1(1,1)=(1/MCS) + ((ALPHA1S^2)*C33S + C11S*ALPHA3S^2 - 2*ALPHA1S*ALPHA3S*C13S)/(C11S*C33S - C13S*C13S);
      Qt1(2,1)=(ALPHA1S.*C33S-ALPHA3S*C13S)/(C11S*C33S-C13S*C13S);
      Qt1(3,1)=(ALPHA3S*C11S-ALPHA1S*C13S)/(C11S*C33S-C13S*C13S);
      Qt1(1,2)=(ALPHA1S*C33S-ALPHA3S*C13S)/(C11S*C33S-(C13S)*(C13S));
      Qt1(2,2)=C33S/(C11S*C33S-C13S*C13S);
      Qt1(3,2)=-C13S/(C11S*C33S-(C13S)*(C13S));
      Qt1(1,3)=(ALPHA3S*C11S-ALPHA1S*C13S)/(C11S*C33S - C13S*C13S);
      Qt1(2,3)=-(C13S)/(C11S*C33S - C13S*C13S);
      Qt1(3,3)=(C11S)/(C11S*C33S - C13S*C13S);
      Qt1(4,4)=1/C55S;
      Qt1(5,5)=RHOSS;
      Qt1(5,7)=RHOFS;
      Qt1(6,6)=RHOSS;
      Qt1(6,8)=RHOFS;
      Qt1(7,5)=RHOFS;
      Qt1(7,7)=m1s;
      Qt1(8,6)=RHOFS;
      Qt1(8,8)=m3s;
      Qt1=inv(Qt1);
      
      Q{1,1}(:,k)=Qt1(1,1);
      Q{2,1}(:,k)=Qt1(2,1);
      Q{3,1}(:,k)=Qt1(3,1);
      Q{1,2}(:,k)=Qt1(1,2);
      Q{2,2}(:,k)=Qt1(2,2);
      Q{3,2}(:,k)=Qt1(3,2);
      Q{1,3}(:,k)=Qt1(1,3);
      Q{2,3}(:,k)=Qt1(2,3);
      Q{3,3}(:,k)=Qt1(3,3);
      Q{4,4}(:,k)=Qt1(4,4);
      Q{5,5}(:,k)=Qt1(5,5);
      Q{5,7}(:,k)=Qt1(5,7);
      Q{6,6}(:,k)=Qt1(6,6);
      Q{6,8}(:,k)=Qt1(6,8);
      Q{7,5}(:,k)=Qt1(7,5);
      Q{7,7}(:,k)=Qt1(7,7);
      Q{8,6}(:,k)=Qt1(8,6);
      Q{8,8}(:,k)=Qt1(8,8);
      
      vmapTop = [vmapTop index(:,k)];
    tag(:,k) = 1;
  end
end
       

















