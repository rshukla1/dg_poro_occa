%%%%%Receiver 2%%%%%%%%%%%

clear all;
clc;
close all;

%%%%%% . SPECFEM PART%%%%%%

zsem=load('zsem_dat.dat');

zsem=zsem(:,2)./(max(abs(zsem(:,2))));
zsem(1760:1950)=5*zsem(1760:1950);

source=load('plot_source_time_function.txt');
t=source(:,1);
t=t-t(1);

%%%%%% . SPECFEM PART%%%%%%



zdg = load('vzdatar2.mat');
zdg=zdg.vzdatar2;
p=1;
zdg=zdg(p,:);

zdg=zdg./(max(abs(zdg)));

dtdg= 1.22e-04;
Ntsteps=9806;
tdg=(1:1:Ntsteps)*dtdg;
tdg=tdg-dtdg;


err=interp1(t,zsem,tdg);
err=err-zdg;
RMSE=rms(err(10:end-1300));



%%%%% PLOT DATA
figure('Renderer', 'painters', 'Position', [10 10 900 600]);
h1=plot(t(1:8:end), -zsem(1:8:end), 'o','MarkerEdgeColor','k','MarkerFaceColor','w');
hold on;
h2=plot(tdg,-zdg,'-k','LineWidth',1.5);

ylim([-1.2,1.2]);
xlim([0, 1.12]);

xlabel('Time (s)', 'FontSize', 24);
ylabel('Normalized vertical velocity {(\it v_3 )}','Interpreter','tex','FontSize', 24);
ax = gca;
ax.FontSize = 24;
legend([h2,h1], 'WADG method','SPECFEM');

text(0.8,-0.75, 'RMSE=6.65e-02', 'FontSize', 22);

text(0.5,0.5, '{(i)}','Interpreter','tex','FontSize', 22);

 text(0.70,0.3, '{(ii) + (iii)}','Interpreter','tex','FontSize', 22);

  text(0.93,0.33, '{(iv)}','Interpreter','tex','FontSize', 22);





