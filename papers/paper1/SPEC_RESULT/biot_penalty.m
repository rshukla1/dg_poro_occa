% function Biot2D
 
clear all -globals
Globals2D
PoroelasticGlobals2D 
N =4;
K1D=212;
c_flag = 0;
FinalTime = 0.25*10^6;
CFL =0.9;
 
[Nv, VX, VY, K, EToV] = unif_tri_mesh(K1D);
StartUp2D;
 
%BuildPeriodicMaps2D(100000,100000);
 
% plotting nodes
[rp sp] = EquiNodes2D(50); [rp sp] = xytors(rp,sp);
Vp = Vandermonde2D(N,rp,sp)/V;
xp = Vp*x; yp = Vp*y;
global xq Vq Pq;

Nq = 2*N+1;
[rq sq wq] = Cubature2D(Nq); % integrate u*v*c
Vq = Vandermonde2D(N,rq,sq)/V;
Pq = V*V'*Vq'*diag(wq); % J's cancel out
Mref = inv(V*V');
xq = Vq*x; yq = Vq*y;
Jq = Vq*J;
 
 
%%%%% params setup
 
U = cell(8,1);
for i = 1:8
    U{i} = zeros(Np,K);
end


global rick ptsrc ptsrcx ptsrcy ;
f0 = 30*(10^-6);tR = 1.20/f0

rick = @(t) (1 - 2*(pi*f0*(t-tR)).^2).*exp(-(pi*f0*(t-tR)).^2);



global invA0
[invA0]=q_matrix();
% Es=Q(1:4, 1:4);
% Ev=Q(5:8, 5:8);
% A0 =blkdiag(Es,Ev);
invA0 = inv(invA0);
 
 
%%%%%%
 
time = 0;
 
% Runge-Kutta residual storage
res = cell(8,1);
for fld = 1:8
    res{fld} = zeros(Np,K);
end
 
% compute time step size
CN = (N+1)^2/2; % guessing...
CNh = max(CN*max(Fscale(:)));
dt = CFL*2/CNh
[ktria, GX,GY]=sample(50000,30000);
srcterm=Sample2DDirac(50000,30000);
ptsrc=zeros(Np,K);
ptsrc(:,ktria)=1;
ptsrc=(V*V')*ptsrc;
[ptsrcx, ptsrcy]=Grad2D(ptsrc);


zrec=40000;
xrec=60000;

[krecv, GX, GY]=sample(xrec,zrec);
tstep=0;
Ntsteps =ceil(FinalTime/dt)
bzdata=zeros(Np,Ntsteps);
bxdata=zeros(Np,Ntsteps);
vzdata=zeros(Np,Ntsteps);
vxdata=zeros(Np,Ntsteps);
vzdatas=zeros(Np,Ntsteps);
vxdatas=zeros(Np,Ntsteps);



while (time<FinalTime)
    tstep=tstep+1
    if(time+dt>FinalTime), dt = FinalTime-time; end

    for INTRK = 1:5
        
        timelocal = time + rk4c(INTRK)*dt;
        
        rhs = RHS2D(U,time);
        
        % initiate and increment Runge-Kutta residuals
        for fld = 1:length(U)
            res{fld} = rk4a(INTRK)*res{fld} + dt*rhs{fld};
            U{fld} = U{fld} + rk4b(INTRK) * res{fld};
        end                
        
    end


     if 0 
        clf
          pp = U{5};
          vv = Vp*pp;
        %         vv = abs(vv);
        color_line3(xp,yp,vv,vv,'.');
        axis equal
        axis tight
        colorbar
%         caxis([-.1 .2])
%         axis([0 1 0 1 -10 10])
        %         PlotField2D(N+1, x, y, pp); view(2)
        title(sprintf('time = %f',time))
        drawnow
    end
 % Increment time
   time = time+dt; 

   bx=U{5} + (RHOF/RHO)*U{7};
   bz=U{6} + (RHOF/RHO)*U{8};
   bzdata(:,tstep)=bz(:, krecv);
   bxdata(:,tstep)=bx(:, krecv);
   vzdata(:,tstep)=U{5}(:, krecv);
   vxdata(:,tstep)=U{6}(:, krecv);
   vzdatas(:,tstep)=U{7}(:, krecv);
   vxdatas(:,tstep)=U{8}(:, krecv);
    if((mod(tstep,6000)==0))
       WriteVTK2D(strcat(strcat('v1_viscid_final_22HZ',num2str(tstep)), '.vtk'), N, {'v1'},U{6});
    end

   if(mod(tstep,500)==0);
       fprintf('At time step %d the max value of v1 is %5.6f\n', tstep, max(max(U{5})));
   end
end
 
 
