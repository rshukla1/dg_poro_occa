from fenics import *
from ufl import nabla_grad, nabla_div


import matplotlib
matplotlib.use('TkAgg')

import matplotlib.pyplot as plt

T = 0.2            # final time
num_steps = 500    # number of time steps
dt = T / num_steps # time step size
lam = 2.0          # diffusion coefficient
mu =  1.0          # diffusion coefficient
rho = 1.0
dt = 0.01
mesh = RectangleMesh(Point(-1,-1), Point(1, 1), 50, 50, "right/left")
V = VectorFunctionSpace(mesh, 'P', 2)
V1= FunctionSpace(mesh, 'P', 2)

u = TrialFunction(V)
v = TestFunction(V)
plot(mesh)

plt.show()



