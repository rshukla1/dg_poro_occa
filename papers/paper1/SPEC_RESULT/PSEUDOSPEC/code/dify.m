function [fyprime]=dify(x)
nx=size(x,1);
hx = ceil(nx/2)-1;
ftdiff = (2i*pi/nx)*(0:hx);
ftdiff(nx:-1:nx-hx+1) = -ftdiff(2:hx+1); 
fyprime=ifft2( bsxfun(@times, fft2(x), ftdiff));
end