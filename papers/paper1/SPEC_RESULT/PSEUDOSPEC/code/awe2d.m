clc;
V=2.0;
FRE=20;
NX=400;
NZ=400;
NT=1200;
dx=V/FRE/20;
dz=dx;
dt=0.5*dx/V;
xs=round(NX/2.5);
zs=round(NZ/2);
t=[0:1:NT]*dt-0.95/FRE;
F2=FRE*FRE;
RICKER=(1-pi^2.*F2*t.*t).*exp(-pi^2.*F2*t.*t);
plot([0:NT]*dt,RICKER);
VEL=ones(NX,NZ)*V;
VEL(NX-round(NX/2):NX,:)=3*VEL(NX-round(NX/2):NX,:);

imagesc(VEL);

data=zeros(NX,NZ);

p0=zeros(NX,NZ);
p1=zeros(NX,NZ);
p2=zeros(NX,NZ);
NX=200; NZ=200;
cns=(dt/dx*VEL).^2;

for it=1:1:NT
    p2=2*p1-p0 +cns.*del2(p1);
    p2(xs,zs)=p2(xs,zs)+RICKER(it);
    data(it,:)=p2(xs,:)';
    p0=p1;
    p1=p2;
    if mod(it,20)==0
        p00=p0/(max(p0(:))+0.001);
        imagesc([1:200]*dx, [1:200]*dz, VEL+p00)
        drawnow
    end
end
    

    




