function [fprime] =fifspec(f,ddx_k_shift_pos,ddy_k_shift_pos, dim)
    global Nx Ny
    fprime = zeros(Nx, Ny);
    fspec=fft2(f);
    if(dim=='x')
        fprime = real(ifft2( bsxfun(@times, ddx_k_shift_pos, fspec)));
    elseif (dim=='y')
      fprime = real(ifft2( bsxfun(@times, ddy_k_shift_pos, fspec)));
    end
return