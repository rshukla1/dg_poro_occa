%clear all;
%clc;
close all;

%%%%%% . SPECFEM PART%%%%%%

zsem=load('vzdataPS.mat');
%save('zsemT0.mat', 'zsem');
zsem=zsem.vzdata;
zsem=zsem./(max(abs(zsem)));

dtsem= 4e-04;%

t=(1:1:length(zsem))*dtsem;
t=t-dtsem;
%figure('Renderer', 'painters', 'Position', [10 10 900 500]);
%%%%%%%%
zsem=load('zsem_data.dat');

zsem=zsem(:,2)./(max(abs(zsem(:,2))));

dt=0.0001;
%t=(1:1:2500)*dt;
%t=t-dt;
source=load('plot_source_time_function.txt');
t=source(:,1);
t=t-t(1);
h1=plot(t(1:10:end),zsem(1:10:end),  'o','MarkerEdgeColor','k','MarkerFaceColor','w');
hold on;



%%%%%% . DFGEM PART%%%%%%
zdg = load('vzdg.mat');
zdg=zdg.vzdata;
p=4;
zdg=zdg(p,:);

zdg=zdg./(max(abs(zdg)));

dtdg= 62.56e-06;
Ntsteps=3996;
tdg=(1:1:Ntsteps)*dtdg;
tdg=tdg-dtdg;
%figure('Renderer', 'painters', 'Position', [10 10 900 500]);
%plot(zdg)
%h1=plot(t(1:1:end), zsem(1:1:end), '-r', 'LineWidth', 1.0);
hold on;
%plot(t_res(1:1:countt),VS_res(1:1:countt)./(max(abs(VS_res(1:countt)))),'--'),xlabel('Time (sec)')


 h2=plot(tdg, -zdg, '-k','LineWidth', 1.0);
% save('zdgfinaldt62p58e-06.mat','zdg');
% save('zdgfinaldtp99e-04.mat','zsem');
legend('SPECFEM', 'DGFEM')

%xlim([0.0, 0.25])
ylim([-1, 1])
zseminterp=interp1(t,zsem,tdg);
TF=isnan(zseminterp);
for i=1:1:length(TF)
    if TF(i)==1
        zseminterp(i)=0;
    end
end
        
err=(zseminterp(100:end)-zdg(100:end))./zdg(100:end);
er=err*err'

return






figure('Renderer', 'painters', 'Position', [10 10 900 500]);
h1=plot(t, y, '-k' , 'LineWidth', 1.0); hold on;
h2=plot(t(1:8:end),ylf(1:8:end),  'o','MarkerEdgeColor','k','MarkerFaceColor','w');
h3=plot(t,10*( y-ylf),'--k', 'Linewidth', 2.0);
ylim([-1.2,1.2]);
xlim([0, 0.35]);
xlabel('Time (s)', 'FontSize', 24);
ylabel('Normalized {\it b_x}','Interpreter','tex','FontSize', 24);
ax = gca;
ax.FontSize = 24;
legend([h1,h2,h3], 'Lax-Freidrich', 'Local Lax-Freidrich', '10 x Resduals ');
max(y-ylf)
%%%%

x=load('bxdata.mat');
xllf=load('bzdataLLF.mat');
y=x.bz;
ylf=xllf.bz;
%zrec=1100;
%xrec=900;
dt=1.7331e-04;
t=[1:1:1731]*dt;
y=y(4,:)./max(abs(y(4,:)));
ylf=ylf(4,:)./max(abs(ylf(4,:)));
figure('Renderer', 'painters', 'Position', [10 10 900 500]);
h1=plot(t, y, '-k' , 'LineWidth', 1.0); hold on;
h2=plot(t(1:8:end),ylf(1:8:end),  'o','MarkerEdgeColor','k','MarkerFaceColor','w');
h3=plot(t,10*( y-ylf),'--k', 'Linewidth', 2.0);
ylim([-1.2,1.2]);
xlim([0, 0.35]);
xlabel('Time (s)', 'FontSize', 24);
ylabel('Normalized {\it b_z}','Interpreter','tex','FontSize', 24);
ax = gca;
ax.FontSize = 24;
legend([h1,h2,h3], 'Lax-Freidrich', 'Local Lax-Freidrich', '10 x Resduals ');
