function [rhsP, rhsSxx,rhsSzz,rhsSxz,rhsvx,rhsvz,rhsqx,rhsqz]=ThermoRhs2D(P,Sxx,Szz,Sxz, vx,vz,qx,qz,time);
global Nx Ny ddx_k_shift_pos ddy_k_shift_pos ;


PoroelasticGlobals2D
dxSxx =fifspec(Sxx,ddx_k_shift_pos,ddy_k_shift_pos,'x' );
dzSxz =fifspec(Sxz,ddx_k_shift_pos,ddy_k_shift_pos,'y' );
dxSxz =fifspec(Sxz,ddx_k_shift_pos,ddy_k_shift_pos,'x' );
dzSzz =fifspec(Szz,ddx_k_shift_pos,ddy_k_shift_pos,'y' );
dxP =fifspec(P,ddx_k_shift_pos,ddy_k_shift_pos,'x' );
dzP =fifspec(P,ddx_k_shift_pos,ddy_k_shift_pos,'y' );

dxVx = fifspec(vx,ddx_k_shift_pos,ddy_k_shift_pos,'x' );
dzVz = fifspec(vz,ddx_k_shift_pos,ddy_k_shift_pos,'y' );
dxQx = fifspec(qx,ddx_k_shift_pos,ddy_k_shift_pos,'x' );
dzQz = fifspec(qz,ddx_k_shift_pos,ddy_k_shift_pos,'y' );

dzVx = fifspec(vx,ddx_k_shift_pos,ddy_k_shift_pos,'y' );
dxVz = fifspec(vz,ddx_k_shift_pos,ddy_k_shift_pos,'x' );


rhsvx=BETA11X*(dxSxx + dzSxz)-BETA12X*(dxP );
rhsvz=BETA11Z*(dxSxz + dzSzz)-BETA12Z*(dzP );

rhsqx=BETA21X*(dxSxx + dzSxz)-BETA22X*(dxP );
rhsqz=BETA21Z*(dxSxz + dzSzz)-BETA22Z*(dzP );


rhsP=-ALPHA1*M*(dxVx)-ALPHA3*M*(dzVz)-M*(dxQx + dzQz);

rhsSxx=C11U*dxVx + C13U*dzVz + ALPHA1*M*(dxQx + dzQz);
rhsSzz=C13U*dxVx + C33U*dzVz + ALPHA3*M*(dxQx + dzQz);
rhsSxz=C55U*(dzVx + dxVz);





% fpeak=25*10^-6;
% 
% wpeak = 2.*pi*fpeak;
% waux  = 0.5*wpeak;
% tdelay = 6./(5.*fpeak);
% tt = time - tdelay;
% trs= exp(-waux*waux*tt*tt/4.)*cos(wpeak*tt);
f0 = 30*(10^-6);
tR = 1.20/f0;
t=time;
trs =  (1 - 2*(pi*f0*(t-tR)).^2).*exp(-(pi*f0*(t-tR)).^2);
SX=301;
SY=501;
rhsSzz(SX,SY)= rhsSzz(SX,SY) + trs;
rhsSxx(SX,SY)= rhsSxx(SX,SY) + trs;
rhsP(SX,SY) =  rhsP(SX,SY) - trs;

% 


return;