function [rhsP, rhsSxx,rhsSzz,rhsSxz,rhsvx,rhsvz,rhsqx,rhsqz]=ThermoRhs2D(P,Sxx,Szz,Sxz, vx,vz,qx,qz,time);
global Nx Ny ddx_k_shift_pos ddy_k_shift_pos ;

PoroelasticGlobals2D;
PoroelasticGlobals2DS;


BETA11XP=zeros(Nx, Ny);
BETA11XP(1:Nx, 1:Ny)=BETA11X;
BETA11XP((Nx/2)+1:end, 1:Ny)=BETA11XS;




BETA12XP=zeros(Nx, Ny);
BETA12XP(1:Nx, 1:Ny)=BETA12X;
BETA12XP((Nx/2)+1:end, 1:Ny)=BETA12XS;



BETA11ZP=zeros(Nx, Ny);
BETA11ZP(1:Nx, 1:Ny)=BETA11Z;
BETA11ZP((Nx/2)+1:end, 1:Ny)=BETA11ZS;


BETA12ZP=zeros(Nx, Ny);
BETA12ZP(1:Nx, 1:Ny)=BETA12Z;
BETA12ZP((Nx/2)+1:end, 1:Ny)=BETA12ZS;





BETA21XP=zeros(Nx, Ny);
BETA21XP(1:Nx, 1:Ny)=BETA21X;
BETA21XP((Nx/2)+1:end, 1:Ny)=BETA21XS;




BETA22XP=zeros(Nx, Ny);
BETA22XP(1:Nx, 1:Ny)=BETA22X;
BETA22XP((Nx/2)+1:end, 1:Ny)=BETA22XS;



BETA21ZP=zeros(Nx, Ny);
BETA21ZP(1:Nx, 1:Ny)=BETA21Z;
BETA21ZP((Nx/2)+1:end, 1:Ny)=BETA21ZS;


BETA22ZP=zeros(Nx, Ny);
BETA22ZP(1:Nx, 1:Ny)=BETA22Z;
BETA22ZP((Nx/2)+1:end, 1:Ny)=BETA22ZS;



ALPHA1P=zeros(Nx, Ny);
ALPHA1P(1:Nx, 1:Ny)=ALPHA1;
ALPHA1P((Nx/2)+1:end, 1:Ny)=ALPHA1S;


ALPHA3P=zeros(Nx, Ny);
ALPHA3P(1:Nx, 1:Ny)=ALPHA3;
ALPHA3P((Nx/2)+1:end, 1:Ny)=ALPHA3S;
imagesc(ALPHA1P);


MP=zeros(Nx, Ny);
MP(1:Nx, 1:Ny)=MC;
MP((Nx/2)+1:end, 1:Ny)=MCS;


C11UP=zeros(Nx, Ny);
C11UP(1:Nx, 1:Ny)=C11U;
C11UP((Nx/2)+1:end, 1:Ny)=C11US;



C13UP=zeros(Nx, Ny);
C13UP(1:Nx, 1:Ny)=C13U;
C13UP((Nx/2)+1:end, 1:Ny)=C13US;
imagesc(C13US);


C33UP=zeros(Nx, Ny);
C33UP(1:Nx, 1:Ny)=C33U;
C33UP((Nx/2)+1:end, 1:Ny)=C33US;




C55UP=zeros(Nx, Ny);
C55UP(1:Nx, 1:Ny)=C55U;
C55UP((Nx/2)+1:end, 1:Ny)=C55US;




PoroelasticGlobals2D
dxSxx =fifspec(Sxx,ddx_k_shift_pos,ddy_k_shift_pos,'x' );
dzSxz =fifspec(Sxz,ddx_k_shift_pos,ddy_k_shift_pos,'y' );
dxSxz =fifspec(Sxz,ddx_k_shift_pos,ddy_k_shift_pos,'x' );
dzSzz =fifspec(Szz,ddx_k_shift_pos,ddy_k_shift_pos,'y' );
dxP =fifspec(P,ddx_k_shift_pos,ddy_k_shift_pos,'x' );
dzP =fifspec(P,ddx_k_shift_pos,ddy_k_shift_pos,'y' );

dxVx = fifspec(vx,ddx_k_shift_pos,ddy_k_shift_pos,'x' );
dzVz = fifspec(vz,ddx_k_shift_pos,ddy_k_shift_pos,'y' );
dxQx = fifspec(qx,ddx_k_shift_pos,ddy_k_shift_pos,'x' );
dzQz = fifspec(qz,ddx_k_shift_pos,ddy_k_shift_pos,'y' );

dzVx = fifspec(vx,ddx_k_shift_pos,ddy_k_shift_pos,'y' );
dxVz = fifspec(vz,ddx_k_shift_pos,ddy_k_shift_pos,'x' );


rhsvx=BETA11XP.*(dxSxx + dzSxz)-BETA12XP.*(dxP );
rhsvz=BETA11ZP.*(dxSxz + dzSzz)-BETA12ZP.*(dzP );

rhsqx=BETA21XP.*(dxSxx + dzSxz)-BETA22XP.*(dxP );
rhsqz=BETA21ZP.*(dxSxz + dzSzz)-BETA22ZP.*(dzP );


rhsP=-ALPHA1P.*M*(dxVx)-ALPHA3P.*M*(dzVz)-MP.*(dxQx + dzQz);

rhsSxx=C11UP.*dxVx + C13UP.*dzVz + ALPHA1P.*MP.*(dxQx + dzQz);
rhsSzz=C13UP.*dxVx + C33UP.*dzVz + ALPHA3P.*MP.*(dxQx + dzQz);
rhsSxz=C55UP.*(dzVx + dxVz);





% fpeak=25*10^-6;
% 
% wpeak = 2.*pi*fpeak;
% waux  = 0.5*wpeak;
% tdelay = 6./(5.*fpeak);
% tt = time - tdelay;
% trs= exp(-waux*waux*tt*tt/4.)*cos(wpeak*tt);
f0 = 10*(10^-6);
tR = 1.20/f0;
t=time;
trs =  (1 - 2*(pi*f0*(t-tR)).^2).*exp(-(pi*f0*(t-tR)).^2);
SX=1451;
SY=801;
rhsSzz(SX,SY)= rhsSzz(SX,SY) + trs;
rhsSxx(SX,SY)= rhsSxx(SX,SY) + trs;
rhsP(SX,SY) =  rhsP(SX,SY) - trs;

% 


return;