function [P,Sxx,Szz,Sxz, vx,vz,qx,qz]=Thermo2D(P,Sxx,Szz,Sxz, vx,vz,qx,qz,FinalTime)
Globals2D;

global Nx Ny kgrid ddx_k_shift_pos ddy_k_shift_pos ;

% Runge-Kutta residual storage  
resvx = zeros(Nx,Ny); 
resvz = zeros(Nx,Ny); 
resSxx = zeros(Nx,Ny); 
resSzz = zeros(Nx,Ny);  
resSxz = zeros(Nx,Ny);  
resqx = zeros(Nx,Ny);  
resqz = zeros(Nx,Ny);  
resP =  zeros(Nx,Ny);



% compute time step size


dt = 500;
time = 0;
Ntsteps=ceil(FinalTime/dt)
vzdata=zeros(Ntsteps,1);
tstep=1;
% outer time step loop 
while (time<FinalTime)
  time
  
  if(time+dt>FinalTime), dt = FinalTime-time; end

   for INTRK = 1:5    
      % compute right hand side of TM-mode Maxwell's equations
      %[rhsvx,rhsvz,rhsSxx,rhsSzz,rhsSxz,rhsT,rhsP]=ThermoRhs2D(vx,vz,Sxx,Szz,Sxz,T,P);
      timelocal = time + rk4c(INTRK)*dt;
      [rhsP, rhsSxx,rhsSzz,rhsSxz,rhsvx,rhsvz,rhsqx,rhsqz]=ThermoRhs2D(P,Sxx,Szz,Sxz, vx,vz,qx,qz,time);
      

      % initiate and increment Runge-Kutta residuals
      resvx = rk4a(INTRK)*resvx + dt*rhsvx;
      resvz = rk4a(INTRK)*resvz + dt*rhsvz;
      resSxx = rk4a(INTRK)*resSxx + dt*rhsSxx; 
      resSzz = rk4a(INTRK)*resSzz + dt*rhsSzz;  
      resSxz = rk4a(INTRK)*resSxz + dt*rhsSxz; 
      resP = rk4a(INTRK)*resP + dt*rhsP;  
      resqx = rk4a(INTRK)*resqx + dt*rhsqx;
      resqz = rk4a(INTRK)*resqz + dt*rhsqz;


      % update fields
      vx  =  vx+rk4b(INTRK)*resvx; 
      vz  =  vz+rk4b(INTRK)*resvz; 
      Sxx =  Sxx+rk4b(INTRK)*resSxx; 
      Szz =  Szz+rk4b(INTRK)*resSzz; 
      Sxz =  Sxz+rk4b(INTRK)*resSxz; 
      P   =  P+rk4b(INTRK)*resP; 
      qx   =  qx+rk4b(INTRK)*resqx;
      qz   =  qz+rk4b(INTRK)*resqz;


   end
   
    

   %pcolor(vz);shading interp;axis square;% caxis([-0.1, 0.1]);
   %hold on;
   %plot(801,1451, 'o','MarkerEdgeColor','k','MarkerFaceColor','w');
   %plot(1000,934, 'x','MarkerEdgeColor','k','MarkerFaceColor','w')
   % colormap(gray)
   %drawnow;
   fprintf("Max value of v3 is %f\n", max(max(vz)));
    vzdata(tstep)=vz(934,1000);
    time = time+dt;
    tstep=tstep+1;
  
end
   save('vzdataPS.mat', 'vzdata');
return
