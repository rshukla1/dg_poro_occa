function [fxprime, fyprime]=difx(x, kx, ky, Nx, Ny)
 % Compute 2D FFT
 data_wavenumberdomain = fft2(x);
 % Compute grid of wavenumbers
 [KX, KY] = meshgrid(kx,ky);
 % Compute 2D derivative
 data_wavenumberdomain_differentiated =  (-1i).*data_wavenumberdomain; 
 % Convert back to space domain
 fxprime= real(ifft2(KX.*data_wavenumberdomain_differentiated ));
 fyprime= real(ifft2(KY.*data_wavenumberdomain_differentiated ));
end


