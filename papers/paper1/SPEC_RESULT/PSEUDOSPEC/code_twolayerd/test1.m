%%% 2D Derivative using Pseudospectral 
clc
clear all;
 Nx = 88;
 Ny = Nx;
 dkx = 1/Nx;
 dky = 1/Ny;
 kx = -2.3: dkx : 2.3; % x-wavenumber
 ky = -2.3 : dky : 2.3; % y-wavenumber
 [X,Y] = meshgrid(kx,ky);
 data_spacedomain = sin(X*2*pi).*cos(Y*2*pi);
 % Compute 2D FFT
 data_wavenumberdomain = fft2(data_spacedomain);
 % Compute grid of wavenumbers
 [KX, KY] = meshgrid(ifftshift(kx),ifftshift(ky));
 
 % Compute 2D derivative
 data_wavenumberdomain_differentiated =  ((1i*2*pi*KY).^2).*data_wavenumberdomain; 
 % Convert back to space domain
 data_spacedomain_differentiated = ifft2(data_wavenumberdomain_differentiated );

 figure(1)
 num=N*N*real(data_spacedomain_differentiated);
 surf( X,Y,num)
 figure(2)
 act=-sin(X*2*pi).*cos(X*2*pi)*2*pi*2*pi;
 err=norm(num-act, 'Fro')
 surf( X,Y, act)