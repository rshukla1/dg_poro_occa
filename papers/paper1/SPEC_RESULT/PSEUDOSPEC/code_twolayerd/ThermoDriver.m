% create the computational grid
close all;
clc;
global Nx Ny ddx_k_shift_pos ddy_k_shift_pos 

Nx = 2402;           % number of grid points in the x (row) direction
Ny = 2402;           % number of grid points in the y (column) direction
dx = 20000;          % grid point spacing in the x direction [m]
dy = 20000;          % grid point spacing in the y direction [m]

% FOR k wave use;
kgrid = kWaveGrid(Nx, dx, Ny, dy);
x=kgrid.x;
y=kgrid.y;
ddx_k_shift_pos = ifftshift( 1i * kgrid.kx_vec );
ddy_k_shift_pos = ifftshift( 1i * kgrid.ky_vec );
ddy_k_shift_pos = ddy_k_shift_pos.';

% dkx=(2*pi)/(Nx*dx);
% dky=(2*pi)/(Ny*dy);
% 
% k1=-dkx*(Nx/2):dkx:-dkx;
% k2= 0:dkx:(dkx*Nx/2-1); 
% kx=[k1 k2]';
% 
% k1=-dky*(Ny/2):dky:-dky;
% k2= 0:dky:(dky*Ny/2-1); 
% ky=[k1 k2]';
% 
% ddx_k_shift_pos = ifftshift( 1i * kx );
% ddy_k_shift_pos = ifftshift( 1i * ky );
% ddy_k_shift_pos = ddy_k_shift_pos.';


FinalTime=1.2*10^6;



P=zeros(Nx,Ny);
Sxx=zeros(Nx, Ny);
Szz=zeros(Nx, Ny);
Sxz=zeros(Nx, Ny);
vx=zeros(Nx, Ny);
vz=zeros(Nx, Ny);
qx=zeros(Nx, Ny);
qz=zeros(Nx, Ny);

%[vx,vz,sxx,szz,sxz,T,P]=Thermo2D(vx,vz,Sxx,Szz,Sxz,T,P);
[P,Sxx,Szz,Sxz, vx,vz,qx,qz]=Thermo2D(P,Sxx,Szz,Sxz, vx,vz,qx,qz,FinalTime);



