clc;
clear;
close all;
Mr=21.6;
Mu=29.4;
f=25;
tau0=1/(2*pi*f);
tets=Mu/Mr;
ts=tau0*(Mr/Mu);
te=ts*(Mu/Mr);
psi=@(t) Mr*(1-(1-tets)*exp(-t/ts));
cai=@(t) (1/Mr)*(1-(1-(1/tets))*exp(-t/te));


t=0:.001:0.050;
figure(1);
plot(t*1000, psi(t),'-r',t*1000, Mr*ones(1,length(t)), '-.b', 'LineWidth', 1.5);
xlabel('Time (ms)'); ylabel('Relaxtion Function (GPa)');
xlim([0,60]); ylim([0,35]);
text(0.2, Mr-2, 'M_R', 'FontSize', 14);
text(0.2, Mu+1, 'M_U', 'FontSize', 14);
figure(2);
plot(t*1000, cai(t),'-r',t*1000, (1/Mr)*ones(1,length(t)), '-.b', 'LineWidth', 1.5);
xlabel('Time (ms)'); ylabel('Creep Function (1/GPa)');
xlim([0,60]); ylim([0,.07]);
text(0.2,0.05, '1/M_R', 'FontSize', 14);
text(0.2, 0.032, '1/M_U', 'FontSize', 14);

clf; close all;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
f=1.5924:1:10^4;
omega=2*pi*f;
cr=3;
cu=3.5;
rho=2.4;
Mr=rho*cr*cr;
Mu=rho*cu*cu;
tets=Mu/Mr;
tau0=1/(2*pi*25);
ts=tau0*(Mr/Mu);
te=ts*(Mu/Mr);
    


Mw=sqrt((1/rho)*(Mr*((1+1j*omega*te)./(1+1j*omega*ts))));
vp=(real(1./Mw)).^-1;
att=-omega.*imag(1./Mw);
Q=(1+(omega.^2)*te*ts)./(omega.*(te-ts));
plot(log10(omega), vp, '-b', 'Linewidth', 2.0);hold on;
plot(log10(omega), sqrt(Mu/rho)*ones(1,length(omega)), '-k', 'Linewidth', 1);
text(1.0,3.58, {'$\sqrt{M_U/\rho}$'}, 'FontSize', 14, 'Interpreter', 'latex');
text(1.0,2.9, {'$\sqrt{M_R/\rho}$'}, 'FontSize', 14, 'Interpreter', 'latex');
ylabel('Phase velocity (km/s)'); xlabel('$\log[\omega (Hz)]$','Interpreter','latex')

%text(0.2, 0.032, '1/M_U', 'FontSize', 14);
ylim([2,4])
xlim([1,6])

close all;
s=1/sqrt(te*ts);

plot(log10(omega), Q.^-1, '-b', 'Linewidth', 2.0);hold on;
plot([log10(s),log10(s)], [0, max(Q.^-1)], '--k', 'LineWidth', 1.0);
ylabel('Dissipation facto'); xlabel('$\log[\omega (Hz)]$','Interpreter','latex')
text(log10(188),0.02, {'$\log(\frac{1}{\tau_0})$'}, 'FontSize', 14, 'Interpreter', 'latex');

%text(0.2, 0.032, '1/M_U', 'FontSize', 14);
%ylim([0,0.2])
%xlim([1,6])


