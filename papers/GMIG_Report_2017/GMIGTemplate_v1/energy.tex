\documentclass{article}

\headheight=8pt \topmargin=0pt
\textheight=624pt \textwidth=432pt
\oddsidemargin=18pt \evensidemargin=18pt

\usepackage{fancyhdr}
\usepackage{amsmath,amssymb}
\usepackage{mathrsfs}
\usepackage{setspace}
\usepackage{graphicx}
\usepackage{color,xcolor}
\usepackage[boxruled,linesnumbered]{algorithm2e}

\include{newcommands}

\title{Biot's notes}

\author{Ruichao Ye}

\begin{document}

\maketitle \setcounter{page}{1} \thispagestyle{fancy}

\allowdisplaybreaks

\section{Weak form}

\begin{align}
    &
    \begin{aligned}
        &
        \DDt{}\int_\Domn \vev \cdot \vew \dd\Domn
        = -\int_\Domn \vetau:\nabla(\frac{m}{r^2}\vew) \dd\Domn
        + \int_\Domn \Psi \veq\cdot(\frac{\rho_f}{r^2}\vew)\dd\Domn
        - \int_\Domn p\nabla\cdot(\frac{\rho_f}{r^2}\vew)\dd\Domn
        \\&\hspace{1cm}
        -\int_\Surf 
        \avg{\ven\cdot\vetau}\cdot\jmp{\frac{m}{r^2}\vew}\dd\Surf
        -\int_\Surf 
        \avg{p}\jmp{\frac{\rho_f}{r^2}\ven\cdot\vew}\dd\Surf
    \end{aligned}
    \label{eq:weak v}
    \\&
    \begin{aligned}
        &
        \DDt{}\int_\Domn \veq\cdot\tilde\vew\dd\Domn
        = \int_\Domn 
        \vetau:\nabla(\frac{\rho_f}{r^2}\tilde\vew)\dd\Domn
        -\int_\Domn
        \Psi\veq\cdot(\frac{\rho}{r^2}\tilde\vew)\dd\Domn
        +\int_\Domn 
        p\nabla\cdot(\frac{\rho}{r^2}\tilde\vew)\dd\Domn
        \\&\hspace{1cm}
        +\int_\Surf
        \avg{\ven\cdot\vetau}\jmp{\frac{\rho_f}{r^2}\tilde\vew}\dd\Surf
        +\int_\Surf
        \avg{p}\jmp{\frac{\rho}{r^2}\ven\cdot\tilde\vew}\dd\Surf
    \end{aligned}
    \label{eq:weak q}
    \\&
    \begin{aligned}
        &
        \DDt{}\int_\Domn \vetau:\tsH\dd\Domn
        = 
        \int_\Domn\nabla\vev:(\tsC^u:\tsH)\dd\Domn
        +\int_\Domn M \alpha (\nabla\cdot\veq)\tsI:\tsH\dd\Domn
        \\&\hspace{1cm}
        +\int_\Surf \jmp{\vev}\cdot\avg{\ven\cdot(\tsC^u:\tsH)}\dd\Surf
        +\int_\Surf \jmp{\ven\cdot\veq}\avg{M\alpha \tsI:\tsH}\dd\Surf
    \end{aligned}
    \label{eq:weak tau}
    \\&
    \begin{aligned}
        &
        \DDt{}\int_\Domn p \tilde H \dd\Domn 
        =-\int_\Domn
        M\alpha(\nabla\cdot\vev)\tilde H \dd\Domn
        -\int_\Domn 
        M(\nabla\cdot\veq)\tilde H \dd\Domn
        \\&\hspace{1cm}
        -\int_\Surf
        \jmp{\ven\cdot\vev}\avg{M\alpha \tilde H}\dd\Surf
        -\int_\Surf
        \jmp{\ven\cdot\veq}\avg{M \tilde H}\dd\Surf
    \end{aligned}
    \label{eq:weak p}
\end{align}

\subsection{Energy estimate}

Let $\tilde\vew=\frac{\rho_f}{\rho}\vew$ in \eqref{eq:weak q},
\begin{align}
    \begin{aligned}
        &
        \DDt{}\int_\Domn \frac{\rho_f}{\rho}\veq\cdot\vew\dd\Domn
        = \int_\Domn 
        \vetau:\nabla(\frac{\rho_f^2}{\rho r^2}\vew)\dd\Domn
        -\int_\Domn
        \Psi\veq\cdot(\frac{\rho_f}{r^2}\vew)\dd\Domn
        +\int_\Domn 
        p\nabla\cdot(\frac{\rho_f}{r^2}\vew)\dd\Domn
        \\&\hspace{1cm}
        +\int_\Surf
        \avg{\ven\cdot\vetau}\jmp{\frac{\rho_f^2}{\rho r^2}\vew}\dd\Surf
        +\int_\Surf
        \avg{p}\jmp{\frac{\rho_f}{r^2}\ven\cdot\vew}\dd\Surf
    \end{aligned}
    \label{eq:weak q1}
\end{align}

\eqref{eq:weak v} $+$ \eqref{eq:weak q1} 
with $r^2=m\rho -\rho_f^2$
yields

\begin{align}
    \begin{aligned}
        &
        \DDt{}\int_\Domn
        \big(\vev+\frac{\rho_f}{\rho}\veq\big)\cdot\vew\dd\Domn
        = -\int_\Domn 
        \vetau:\nabla\big(\frac1{\rho}\vew\big)
        \dd\Domn
        -\int_\Surf
        \avg{\ven\cdot\vetau}\cdot
        \jmp{\frac1{\rho}\vew}\dd\Surf
    \end{aligned}
    \label{eq:weak v1}
\end{align}

Let $\vew=\frac{\rho\rho_f}{r^2}\tilde\vew$ in \eqref{eq:weak v1},

\begin{align}
    \begin{aligned}
        &
        \DDt{}\int_\Domn
        \big(\vev+\frac{\rho_f}{\rho}\veq\big)\cdot
        \big(\frac{\rho\rho_f}{r^2}\tilde\vew\big)\dd\Domn
        = -\int_\Domn 
        \vetau:\nabla\big(\frac{\rho_f}{r^2}\tilde\vew\big)
        \dd\Domn
        -\int_\Surf
        \avg{\ven\cdot\vetau}\cdot
        \jmp{\frac{\rho_f}{r^2}\tilde\vew}\dd\Surf
    \end{aligned}
    \label{eq:weak v2}
\end{align}

\eqref{eq:weak q}$+$\eqref{eq:weak v2}
with $r^2=m\rho -\rho_f^2$
yields

\begin{align}
    \begin{aligned}
        &
        \DDt{}\int_\Domn
        \big(\frac{\rho\rho_f}{r^2}\vev+\frac{m\rho}{r^2}\veq
        \big)
        \cdot\tilde\vew\dd\Domn
        =
        -\int_\Domn
        \Psi\veq\cdot(\frac{\rho}{r^2}\tilde\vew)\dd\Domn
        +\int_\Domn 
        p\nabla\cdot(\frac{\rho}{r^2}\tilde\vew)\dd\Domn
        \\&\hspace{1cm}
        +\int_\Surf
        \avg{p}\jmp{\frac{\rho}{r^2}\ven\cdot\tilde\vew}\dd\Surf
    \end{aligned}
    \label{eq:weak q2}
\end{align}

Let $\tilde H=\alpha \tsI:\tsH$ in \eqref{eq:weak p},
\begin{align}
    \begin{aligned}
        &
        \DDt{}\int_\Domn \alpha p \tsI:\tsH \dd\Domn 
        =-\int_\Domn
        M\alpha^2(\nabla\cdot\vev)\tsI:\tsH \dd\Domn
        -\int_\Domn 
        M\alpha (\nabla\cdot\veq)\tsI:\tsH \dd\Domn
        \\&\hspace{1cm}
        -\int_\Surf
        \jmp{\ven\cdot\vev}\avg{M\alpha^2 \tsI:\tsH}\dd\Surf
        -\int_\Surf
        \jmp{\ven\cdot\veq}\avg{M \alpha \tsI:\tsH}\dd\Surf
    \end{aligned}
    \label{eq:weak p1}
\end{align}

\eqref{eq:weak tau}$+$\eqref{eq:weak p1} yields
\begin{align}
    \begin{aligned}
        &
        \DDt{}\int_\Domn (\vetau+\alpha p \tsI):\tsH \dd\Domn 
        =
        \int_\Domn\nabla\vev:(\tsC^u:\tsH)\dd\Domn
        -\int_\Domn M\alpha^2(\nabla\cdot\vev)\tsI:\tsH \dd\Domn
        \\&\hspace{1cm}
        +\int_\Surf \jmp{\vev}\cdot\avg{\ven\cdot(\tsC^u:\tsH)}\dd\Surf
        -\int_\Surf
        \jmp{\ven\cdot\vev}\avg{M\alpha^2 \tsI:\tsH}\dd\Surf
    \end{aligned}
    \label{eq:weak tau1}
\end{align}

Let $\tsH=\big((\tsC^u)^{-1}:\tsI\big)M\alpha\tilde H$ in 
\eqref{eq:weak tau},
\begin{align}
    \begin{aligned}
        &
        \DDt{}\int_\Domn 
        \big(\vetau:(\tsC^u)^{-1}:\tsI\big)M\alpha\tilde H\dd\Domn
        = 
        \\&\hspace{1cm}
        \int_\Domn(\nabla\cdot\vev)M\alpha\tilde H\dd\Domn
        +\int_\Domn M^2\alpha^2 (\nabla\cdot\veq)
        \big(\tsI:(\tsC^u)^{-1}:\tsI\big)\tilde H\dd\Domn
        \\&\hspace{1cm}
        +\int_\Surf \jmp{\vev\cdot\ven}\avg{M\alpha\tilde H}\dd\Surf
        +\int_\Surf 
        \jmp{\ven\cdot\veq}
        \avg{M^2\alpha^2 \big(\tsI:(\tsC^u)^{-1}:\tsI\big)\tilde H}
        \dd\Surf
    \end{aligned}
    \label{eq:weak tau2}
\end{align}

\eqref{eq:weak p}$+$\eqref{eq:weak tau2} yields
\begin{align}
    \begin{aligned}
        &
        \DDt{}\int_\Domn \Big(
        \big(\vetau:(\tsC^u)^{-1}:\tsI\big)M\alpha+
        p\Big) \tilde H \dd\Domn 
        = 
        \\&\hspace{1cm}
        \int_\Domn \Big(
        M^2\alpha^2 \big(\tsI:(\tsC^u)^{-1}:\tsI\big)
        -M\Big)
        (\nabla\cdot\veq)\tilde H\dd\Domn
        \\&\hspace{1cm}
        +\int_\Surf \jmp{\ven\cdot\veq}
        \avg{\big(M^2\alpha^2 \big(\tsI:(\tsC^u)^{-1}:\tsI\big)-M\big)
        \tilde H}\dd\Surf
    \end{aligned}
    \label{eq:weak p2}
\end{align}


Let $\vew=\rho\vev$ in \eqref{eq:weak v1}, 
$\tilde\vew=\frac{r^2}\rho\veq$ in \eqref{eq:weak q2},
$\tsH=(\tsC^u)^{-1}:\vetau$ in \eqref{eq:weak tau1}, and 
$\tilde H=M^{-1}p$ in \eqref{eq:weak p2} yields
\begin{align}
    &
    \begin{aligned}
        &
        \DDt{}\int_\Domn
        \big(\rho\vev+\rho_f\veq\big)\cdot\vev\dd\Domn
        = 
{\Cred
        -\int_\Domn \vetau:\nabla\vev \dd\Domn
        -\int_\Surf \avg{\ven\cdot\vetau}\cdot \jmp{\vev}\dd\Surf
}
    \end{aligned}
    \label{eq:eng v}
    \\&
    \begin{aligned}
        &
        \DDt{}\int_\Domn \big(\rho_f\vev+m\veq
        \big)\cdot\veq\dd\Domn
        =
        -\int_\Domn (\Psi\veq)\cdot\veq\dd\Domn
{\Cred
        +\int_\Domn p\nabla\cdot\veq\dd\Domn
        +\int_\Surf \avg{p}\jmp{\ven\cdot\veq}\dd\Surf
}
    \end{aligned}
    \label{eq:eng q}
    \\&
    \begin{aligned}
        &
        \DDt{}\int_\Domn (\vetau+\alpha p \tsI):(\tsC^u)^{-1}:\vetau
        \dd\Domn 
        =
{\Cred
        \int_\Domn\nabla\vev:\vetau\dd\Domn
}
{\Cblu
        -\int_\Domn M\alpha^2(\nabla\cdot\vev)\tsI:(\tsC^u)^{-1}:\vetau 
        \dd\Domn
}
        \\&\hspace{1cm}
{\Cred
        +\int_\Surf \jmp{\vev}\cdot\avg{\ven\cdot\vetau}\dd\Surf
}
{\Cblu
        -\int_\Surf
        \jmp{\ven\cdot\vev}\avg{M\alpha^2 \tsI:(\tsC^u)^{-1}:\vetau}
        \dd\Surf
}
    \end{aligned}
    \label{eq:eng tau}
    \\&
    \begin{aligned}
        &
        \DDt{}\int_\Domn \Big(
        \big(\vetau:(\tsC^u)^{-1}:\tsI\big)p\alpha+
        \frac{p^2}M \Big) \dd\Domn 
        = 
        \int_\Domn M\alpha^2 (\nabla\cdot\veq)
        \big(\tsI:(\tsC^u)^{-1}:\tsI\big) p \dd\Domn
{\Cred
        -\int_\Domn (\nabla\cdot\veq) p \dd\Domn
}
        \\&\hspace{1cm}
        +\int_\Surf \jmp{\ven\cdot\veq}
        \avg{M\alpha^2 \big(\tsI:(\tsC^u)^{-1}:\tsI\big) p}
        \dd\Surf
{\Cred
        -\int_\Surf \jmp{\ven\cdot\veq}\avg{p}\dd\Surf
}
    \end{aligned}
    \label{eq:eng p}
\end{align}

Let $\tilde H=-\alpha\big(\tsI:(\tsC^u)^{-1}:\vetau\big)$ 
in \eqref{eq:weak p} yields
\begin{align}
    \begin{aligned}
        &
        -\DDt{}\int_\Domn 
        \alpha\big(\tsI:(\tsC^u)^{-1}:\vetau\big)p \dd\Domn 
        =
{\Cblu
        \int_\Domn
        M\alpha^2(\nabla\cdot\vev)
        \big(\tsI:(\tsC^u)^{-1}:\vetau\big)\dd\Domn
}
        \\&\hspace{1cm}
        +\int_\Domn 
        M\alpha(\nabla\cdot\veq)\big(\tsI:(\tsC^u)^{-1}:\vetau\big)
        \dd\Domn
{\Cblu
        +\int_\Surf
        \jmp{\ven\cdot\vev}\avg{M\alpha^2 
            \big(\tsI:(\tsC^u)^{-1}:\vetau\big)}\dd\Surf
}
        \\&\hspace{1cm}
        +\int_\Surf
        \jmp{\ven\cdot\veq}
        \avg{M\alpha\big(\tsI:(\tsC^u)^{-1}:\vetau\big)}\dd\Surf
    \end{aligned}
    \label{eq:weak p3}
\end{align}

Let 
\[
\tilde H=
\beta\Big(
\alpha\big(\tsI:(\tsC^u)^{-1}:\tsI\big)p
+\big(\tsI:(\tsC^u)^{-1}:\vetau\big)
\Big)
\]
with 
\[
\beta=\Big(
\alpha^{-1}-M\alpha \big(\tsI:(\tsC^u)^{-1}:\tsI\big)
\Big)^{-1}
\]
in \eqref{eq:weak p2},
\begin{align}
    \begin{aligned}
        &
        \DDt{}\int_\Domn 
        \beta\Big(
        \alpha\big(\tsI:(\tsC^u)^{-1}:\tsI\big)p
        +\big(\tsI:(\tsC^u)^{-1}:\vetau\big)
        \Big)
        \Big(
        \big(\vetau:(\tsC^u)^{-1}:\tsI\big)M\alpha+
        p\Big) \dd\Domn 
        = 
        \\&\hspace{1cm}
        -\int_\Domn 
        M\alpha 
        \Big(\alpha\big(\tsI:(\tsC^u)^{-1}:\tsI\big)p
        +\big(\tsI:(\tsC^u)^{-1}:\vetau\big)\Big)
        (\nabla\cdot\veq)\dd\Domn
        \\&\hspace{1cm}
        -\int_\Surf \jmp{\ven\cdot\veq}
        \avg{M\alpha
        \Big(\alpha\big(\tsI:(\tsC^u)^{-1}:\tsI\big)p
        +\big(\tsI:(\tsC^u)^{-1}:\vetau\big)\Big)
        }\dd\Surf
    \end{aligned}
    \label{eq:weak p3}
\end{align}

Summerize left-hand-sides of \eqref{eq:eng tau}--\eqref{eq:weak p3}
yields
\begin{align}
    \begin{aligned}
        &
        \DDt{}\int_\Domn \bigg(
        (\vetau+\alpha p \tsI):(\tsC^u)^{-1}:\vetau
        +\big(\vetau:(\tsC^u)^{-1}:\tsI\big)p\alpha
        +\frac{p^2}M  
        -\alpha\big(\tsI:(\tsC^u)^{-1}:\vetau\big)p 
        \\&\hspace{1.5cm}
        +\beta\Big(
        \alpha\big(\tsI:(\tsC^u)^{-1}:\tsI\big)p
        +\big(\tsI:(\tsC^u)^{-1}:\vetau\big)
        \Big)
        \Big(
        \big(\vetau:(\tsC^u)^{-1}:\tsI\big)M\alpha+
        p\Big) 
        \bigg)\dd\Domn 
        \\&\hspace{1cm}
        =
        \DDt{}\int_\Domn \bigg(
        (\vetau+\alpha p \tsI):(\tsC^u)^{-1}:\vetau
        +\frac{p^2}M  
        \\&\hspace{2cm}
        +\beta\Big(
        \big( (\tsI:(\tsC^u)^{-1}:\tsI)M\alpha^2+1\big)
        \big(\vetau:(\tsC^u)^{-1}:\tsI\big)p
        \\&\hspace{2.5cm}
        +M\alpha\big(\tsI:(\tsC^u)^{-1}:\vetau\big)^2
        +\alpha\big(\tsI:(\tsC^u)^{-1}:\tsI\big)p^2
        \Big)
        \bigg)\dd\Domn 
        \\&\hspace{1cm}
        =
        \DDt{}\int_\Domn \bigg(
        \vetau:(\tsC^u)^{-1}:\vetau
        +\alpha p \big(\tsI:(\tsC^u)^{-1}:\vetau\big)
        +\frac{p^2}M  
        \\&\hspace{2cm}
        +\beta
        \Big(\big(\tsI:(\tsC^u)^{-1}:\tsI\big)M\alpha^2+1\Big)
        \big(\vetau:(\tsC^u)^{-1}:\tsI\big)p
        \\&\hspace{2cm}
        +\beta
        M\alpha\big(\tsI:(\tsC^u)^{-1}:\vetau\big)^2
        +\beta
        \alpha\big(\tsI:(\tsC^u)^{-1}:\tsI\big)p^2
        \bigg)\dd\Domn 
        \\&\hspace{1cm}
        =
        \DDt{}\int_\Domn \bigg(
        \vetau:(\tsC^u)^{-1}:\vetau
        +\beta M\alpha\big(\tsI:(\tsC^u)^{-1}:\vetau\big)^2
        \\&\hspace{2cm}
        +\frac{\beta}{M\alpha}p^2
        +2\beta \big(\vetau:(\tsC^u)^{-1}:\tsI\big)p
        \bigg)\dd\Domn 
        \\&\hspace{1cm}
    \end{aligned}
    \label{eq:potential energy}
\end{align}

Summerize \eqref{eq:eng v}--\eqref{eq:weak p3} thus yields

\begin{align}
    \begin{aligned}
        &
        \DDt{}\int_\Domn\bigg(
        \rho\vev^2+2\rho_f\veq\cdot\vev
        +m\veq^2
        +\vetau:(\tsC^u)^{-1}:\vetau
        +\beta M\alpha\big(\tsI:(\tsC^u)^{-1}:\vetau\big)^2
        \\&\hspace{2cm}
        +\frac{\beta}{M\alpha}p^2
        +2\beta \big(\vetau:(\tsC^u)^{-1}:\tsI\big)p
        \bigg)\dd\Domn
        =
        -\int_\Domn (\Psi\veq)\cdot\veq\dd\Domn
    \end{aligned}
    \label{eq:energy}
\end{align}

\subsection{Energy estimate: an equivalent approach}

Let $\vew=\rho\vev +\rho_f\veq$ in \eqref{eq:weak v},
$\tilde \vew=\rho_f \vev +m\veq$
in \eqref{eq:weak q}, such that
\begin{align}
    &
    \begin{aligned}
        &
        \DDt{}\int_\Domn \vev \cdot
        \big(\rho\vev +\rho_f\veq\big)\dd\Domn
        = 
{\Cred
        \int_\Domn 
        \frac{\rho\rho_f}{r^2}
        \Psi \veq\cdot\vev 
        \dd\Domn
}
        + \int_\Domn 
        \frac{\rho_f^2}{r^2}
        (\Psi \veq)\cdot \veq\dd\Domn
        \\&\hspace{1cm}
        -\int_\Domn \vetau:\nabla\big(
        \frac{m\rho}{r^2}\vev 
{\Cred
        +\frac{m\rho_f}{r^2}\veq
}
        \big) \dd\Domn
        - \int_\Domn p\nabla\cdot\big(
{\Cred
        \frac{\rho\rho_f}{r^2}\vev 
}
        +\frac{\rho_f^2}{r^2}\veq\big)\dd\Domn
        \\&\hspace{1cm}
        -\int_\Surf 
        \avg{\ven\cdot\vetau}\cdot\jmp{
        \frac{m\rho}{r^2}\vev 
{\Cred
        +\frac{m\rho_f}{r^2}\veq
}
        }\dd\Surf
        -\int_\Surf 
        \avg{p}\ven\cdot\jmp{
{\Cred
        \frac{\rho\rho_f}{r^2}\vev 
}
        +\frac{\rho_f^2}{r^2}\veq
        }\dd\Surf
    \end{aligned}
    \label{eq:Eng v}
    \\&
    \begin{aligned}
        &
        \DDt{}\int_\Domn \veq\cdot
        (\rho_f \vev +m\veq)
        \dd\Domn
        = 
{\Cred
        -\int_\Domn
        \frac{\rho\rho_f}{r^2} \Psi\veq\cdot\vev\dd\Domn
}
        -\int_\Domn
        \frac{m\rho}{r^2}(\Psi\veq)\cdot\veq \dd\Domn
        \\&\hspace{1cm}
        +\int_\Domn \vetau:\nabla\big(
        \frac{\rho_f^2}{r^2} \vev 
{\Cred
        +\frac{m\rho_f}{r^2}\veq
}
        \big)\dd\Domn
        +\int_\Domn p\nabla\cdot\big(
{\Cred
        \frac{\rho\rho_f}{r^2} \vev 
}
        +\frac{m\rho}{r^2}\veq
        \big)\dd\Domn
        \\&\hspace{1cm}
        +\int_\Surf
        \avg{\ven\cdot\vetau}\jmp{
        \frac{\rho_f^2}{r^2} \vev 
{\Cred
        +\frac{m\rho_f}{r^2}\veq
}
        }\dd\Surf
        +\int_\Surf
        \avg{p}\ven\cdot\jmp{
{\Cred
        \frac{\rho\rho_f}{r^2} \vev 
}
        +\frac{m\rho}{r^2}\veq
        }\dd\Surf
    \end{aligned}
    \label{eq:Eng q}
\end{align}
With $r^2=m\rho -\rho_f^2$, \eqref{eq:Eng v} $+$ \eqref{eq:Eng q}
yields
\begin{align}
    \begin{aligned}
        &
        \DDt{}\int_\Domn 
        \big(\rho\vev^2 +2\rho_f\vev \cdot\veq+m\veq^2\big)
        \dd\Domn
        = 
        -\int_\Domn 
        (\Psi \veq)\cdot \veq\dd\Domn
\Cblu
        -\int_\Domn \vetau:\nabla\vev \dd\Domn
        +\int_\Domn p\nabla\cdot\veq \dd\Domn
        \\&\hspace{1cm}
\Cblu
        -\int_\Surf 
        \avg{\ven\cdot\vetau}\cdot\jmp{\vev}\dd\Surf
        +\int_\Surf
        \avg{p}\jmp{\ven\cdot\veq}\dd\Surf
    \end{aligned}
    \label{eq:Eng kinatic}
\end{align}

Let $
\tsH=
\Big((\tsC^u)^{-1}:\vetau
+\big( (\tsC^u)^{-1}:\tsI \big) M\alpha\beta
\big(\tsI:(\tsC^u)^{-1}:\vetau\big)
+\beta\big( (\tsC^u)^{-1}:\tsI \big)p\Big)
$ in \eqref{eq:weak tau}, and $
\tilde H =
\Big( \beta \big(\tsI:(\tsC^u)^{-1}:\vetau\big)
+\frac\beta{M\alpha}p\Big)
$ in \eqref{eq:weak p}, such that
\begin{align}
    &
    \begin{aligned}
        &
        \DDt{}\int_\Domn 
        \Big(\vetau:(\tsC^u)^{-1}:\vetau
        +M\alpha\beta
        \big(\tsI:(\tsC^u)^{-1}:\vetau\big)^2
        +\beta\big( \tsI:(\tsC^u)^{-1}:\vetau \big)p\Big)
        \dd\Domn
        \\&\hspace{1cm}
        = 
        \int_\Domn
        \nabla\vev:\vetau
        \dd\Domn
{\Cred
        + \int_\Domn \Big(
        M\alpha\beta\big(\nabla\cdot\vev\big)
        \big(\tsI:(\tsC^u)^{-1}:\vetau\big)
        +\beta\big(\nabla\cdot\vev\big)p\Big)
        \dd\Domn
}
        \\&\hspace{1cm}
        +\int_\Domn 
        \Big(1+M\alpha\beta\big( \tsI:(\tsC^u)^{-1}:\tsI \big)\Big)
        M \alpha (\nabla\cdot\veq)\big(\tsI:(\tsC^u)^{-1}:\vetau\big)
        \dd\Domn
        \\&\hspace{1cm}
        +\int_\Domn 
        \beta M \alpha \big(\tsI:(\tsC^u)^{-1}:\tsI \big)
        (\nabla\cdot\veq)p
        \dd\Domn
        +\int_\Surf \jmp{\vev}\cdot\avg{\ven\cdot\vetau}\dd\Surf
        \\&\hspace{1cm}
{\Cred
        +\int_\Surf \jmp{\ven\cdot\vev}\avg{
        M\alpha\beta
        \big(\tsI:(\tsC^u)^{-1}:\vetau\big)
        }\dd\Surf
        +\int_\Surf \jmp{\ven\cdot\vev}\avg{\beta p}\dd\Surf
}
        \\&\hspace{1cm}
        +\int_\Surf \jmp{\ven\cdot\veq}\avg{M\alpha 
        \big(1
        + M\alpha\beta\big( \tsI:(\tsC^u)^{-1}:\tsI \big)
        \big)
        \big(\tsI:(\tsC^u)^{-1}:\vetau\big)
        }\dd\Surf
        \\&\hspace{1cm}
        +\int_\Surf \jmp{\ven\cdot\veq}\avg{M\alpha 
        \Big(
        \beta\big( \tsI:(\tsC^u)^{-1}:\tsI \big)p\Big)
        }\dd\Surf
    \end{aligned}
    \label{eq:Eng tau}
    \\&
    \begin{aligned}
        &
        \DDt{}\int_\Domn  
        \Big( \beta \big(\tsI:(\tsC^u)^{-1}:\vetau\big)p
        +\frac\beta{M\alpha}p^2\Big)
        \dd\Domn 
        =
        \\&\hspace{1cm}
{\Cred
        -\int_\Domn\Big( 
        M\alpha\beta 
        (\nabla\cdot\vev)\big(\tsI:(\tsC^u)^{-1}:\vetau\big)
        +\beta(\nabla\cdot\vev)p\Big)
        \dd\Domn
}
        \\&\hspace{1cm}
        -\int_\Domn \Big( 
        M\beta 
        (\nabla\cdot\veq)\big(\tsI:(\tsC^u)^{-1}:\vetau\big)
        +\frac\beta{\alpha}(\nabla\cdot\veq)p\Big)
        \dd\Domn
        \\&\hspace{1cm}
{\Cred
        -\int_\Surf
        \jmp{\ven\cdot\vev}\avg{ 
        M\alpha\beta \big(\tsI:(\tsC^u)^{-1}:\vetau\big)
        +\beta p
        }\dd\Surf
}
        \\&\hspace{1cm}
        -\int_\Surf
        \jmp{\ven\cdot\veq}\avg{ 
        M\beta \big(\tsI:(\tsC^u)^{-1}:\vetau\big)
        +\frac\beta{\alpha}p
        }\dd\Surf
    \end{aligned}
    \label{eq:Eng p}
\end{align}

With 
\[
\beta=\Big(
\alpha^{-1}-M\alpha \big(\tsI:(\tsC^u)^{-1}:\tsI\big)
\Big)^{-1},
\]
\eqref{eq:Eng tau}$+$\eqref{eq:Eng p} yields
\begin{align}
    \begin{aligned}
        &
        \DDt{}\int_\Domn 
        \Big(\vetau:(\tsC^u)^{-1}:\vetau
        +M\alpha\beta
        \big(\tsI:(\tsC^u)^{-1}:\vetau\big)^2
        +\beta\big( \tsI:(\tsC^u)^{-1}:\vetau \big)p\Big)
        \dd\Domn
        \\&\hspace{1cm}
        +\DDt{}\int_\Domn  
        \Big( \beta \big(\tsI:(\tsC^u)^{-1}:\vetau\big)p
        +\frac\beta{M\alpha}p^2\Big)
        \dd\Domn 
        \\&\hspace{0.5cm}
        = 
\Cblu
        \int_\Domn
        \nabla\vev:\vetau
        \dd\Domn
        -\int_\Domn 
        (\nabla\cdot\veq)p
        \dd\Domn
        \\&\hspace{1cm}
\Cred
        +\int_\Domn \Big(
        \beta^{-1}+M\alpha\big(\tsI:(\tsC^u)^{-1}:\tsI\big)-\alpha^{-1}
        \Big)
        M\alpha\beta(\nabla\cdot\veq)\big(\tsI:(\tsC^u)^{-1}:\vetau\big)
        \dd\Domn
        \\&\hspace{1cm}
\Cred
        +\int_\Domn \beta\Big(
        M\alpha\big(\tsI:(\tsC^u)^{-1}:\tsI \big)
        -\alpha^{-1}+\beta^{-1}
        \Big)
        (\nabla\cdot\veq)p
        \dd\Domn
        \\&\hspace{1cm}
\Cblu
        +\int_\Surf \jmp{\vev}\cdot\avg{\ven\cdot\vetau}\dd\Surf
        -\int_\Surf \jmp{\ven\cdot\veq}\avg{p}\dd\Surf
        \\&\hspace{1cm}
\Cred
        +\int_\Surf \jmp{\ven\cdot\veq}\avg{M\alpha\beta
        \big(\beta^{-1}+ M\alpha\big(\tsI:(\tsC^u)^{-1}:\tsI
        \big)-\alpha^{-1}\big)
        \big(\tsI:(\tsC^u)^{-1}:\vetau\big)
        }\dd\Surf
        \\&\hspace{1cm}
\Cred
        +\int_\Surf \jmp{\ven\cdot\veq}\avg{\beta\big(
        M\alpha\big( \tsI:(\tsC^u)^{-1}:\tsI \big)
        -\alpha^{-1}+\beta^{-1}
        \big) p }\dd\Surf.
    \end{aligned}
    \label{eq:Eng dynamic}
\end{align}

\eqref{eq:Eng kinatic}$+$\eqref{eq:Eng dynamic} yields
the same result as \eqref{eq:energy}

\section{Penalty flux}

We recall $\jmp{\rho\vev}=\jmp{\rho}\avg{\vev}+\avg{\rho}\jmp{\vev}$.
Assume
\begin{align}
    \begin{aligned}
        &
    \Big(\jmp{\rho}\avg{\vev}+\avg{\rho}\jmp{\vev}+
    \jmp{\rho_f}\avg{\veq}+\avg{\rho_f}\jmp{\veq}\Big)\cdot
    \\&\hspace{2cm}
    \Big(a_1\avg{\vev}+a_2\jmp{\vev}+a_3\avg{\veq}+a_4\jmp{\veq}\Big)
    \\&\hspace{0.5cm}
    +
    \Big(\jmp{\rho_f}\avg{\vev}+\avg{\rho_f}\jmp{\vev}+
    \jmp{m}\avg{\veq}+\avg{m}\jmp{\veq}\Big)\cdot
    \\&\hspace{2cm}
    \Big(a_5\avg{\vev}+a_6\jmp{\vev}+a_7\avg{\veq}+a_8\jmp{\veq}\Big)
    \\&\hspace{0.5cm}
    =b_1\jmp{\vev}^2+b_2\jmp{\veq}^2+b_3\jmp{\vev}\cdot\jmp{\veq}
    \end{aligned}
    \label{}
\end{align}
holds for any $\vev$ and $\veq$, thus yields
\begin{align}
    \begin{aligned}
    \jmp{\rho}a_1+\jmp{\rho_f}a_5 
    = &\,0 , \\
    \jmp{\rho_f}a_3+\jmp{m}a_7
    = &\,0 , \\
    \avg{\rho}a_1+\jmp{\rho}a_2+\avg{\rho_f}a_5+\jmp{\rho_f}a_6
    = &\,0 , \\
    \jmp{\rho_f}a_1+\jmp{\rho}a_3+\jmp{m}a_5+\jmp{\rho_f}a_7
    = &\,0 , \\
    \avg{\rho_f}a_1+\jmp{\rho}a_4+\avg{m}a_5+\jmp{\rho_f}a_8
    = &\,0 , \\
    \jmp{\rho_f}a_2+\avg{\rho}a_3+\jmp{m}a_6+\avg{\rho_f}a_7
    = &\,0 , \\
    \avg{\rho_f}a_3+\jmp{\rho_f}a_4+\avg{m}a_7+\jmp{m}a_8
    = &\,0 ,
    \end{aligned}
    \label{eq:penalty coeff}
\end{align}
and
\begin{align}
    \begin{aligned}
    \avg{\rho}a_2+\avg{\rho_f}a_6+b_1
    = &\,0 , \\
    \avg{\rho_f}a_4+\avg{m}a_8+b_2
    = &\,0 , \\
    \avg{\rho_f}a_2+\avg{\rho}a_4+\avg{m}a_6+\avg{\rho_f}a_8+b_3
    = &\,0 .
    \end{aligned}
    \label{}
\end{align}
To make a penalty scheme holds, it is also required 
$b_1 > 0,b_2 > 0,4b_1b_2\geq b_3^2$.
System \eqref{eq:penalty coeff} is over-determinent,
thus one should be able to obtain solutions for $b_1,b_2$ and $b_3$.

A penalty modification for \eqref{eq:weak v} and \eqref{eq:weak q}
are then given by
\begin{align}
    &
    \begin{aligned}
        &
        \DDt{}\int_\Domn \vev \cdot \vew \dd\Domn
        = -\int_\Domn \vetau:\nabla(\frac{m}{r^2}\vew) \dd\Domn
        + \int_\Domn \Psi \veq\cdot(\frac{\rho_f}{r^2}\vew)\dd\Domn
        - \int_\Domn p\nabla\cdot(\frac{\rho_f}{r^2}\vew)\dd\Domn
        \\&\hspace{1cm}
        -\int_\Surf 
        \avg{\ven\cdot\vetau}\cdot\jmp{\frac{m}{r^2}\vew}\dd\Surf
        -\int_\Surf 
        \avg{p}\jmp{\frac{\rho_f}{r^2}\ven\cdot\vew}\dd\Surf
        \\&\hspace{1cm}
        +\int_\Surf \Big(
        a_1\avg{\vev}+a_2\jmp{\vev}
        +a_3\avg{\ven\cdot\veq}\ven+a_4\jmp{\ven\cdot\veq}\ven\Big)
        \cdot \jmp{\vew} \dd\Surf,
    \end{aligned}
    \label{eq:penalty v}
    \\&
    \begin{aligned}
        &
        \DDt{}\int_\Domn \veq\cdot\tilde\vew\dd\Domn
        = \int_\Domn 
        \vetau:\nabla(\frac{\rho_f}{r^2}\tilde\vew)\dd\Domn
        -\int_\Domn
        \Psi\veq\cdot(\frac{\rho}{r^2}\tilde\vew)\dd\Domn
        +\int_\Domn 
        p\nabla\cdot(\frac{\rho}{r^2}\tilde\vew)\dd\Domn
        \\&\hspace{1cm}
        +\int_\Surf
        \avg{\ven\cdot\vetau}\jmp{\frac{\rho_f}{r^2}\tilde\vew}\dd\Surf
        +\int_\Surf
        \avg{p}\jmp{\frac{\rho}{r^2}\ven\cdot\tilde\vew}\dd\Surf
        \\&\hspace{1cm}
        +\int_\Surf\Big(
        a_5\avg{\vev}+a_6\jmp{\vev}
        +a_7\avg{\ven\cdot\veq}\ven+a_8\jmp{\ven\cdot\veq}\ven\Big)
        \cdot \jmp{\tilde\vew} \dd\Surf,
    \end{aligned}
    \label{eq:penalty q}
\end{align}
and the energy estimate then yields
\begin{align}
    \begin{aligned}
        &
        \DDt{}\int_\Domn\bigg(
        \rho\vev^2+2\rho_f\veq\cdot\vev
        +m\veq^2
        +\vetau:(\tsC^u)^{-1}:\vetau
        \\&\hspace{2cm}
        +\beta M\alpha\big(\tsI:(\tsC^u)^{-1}:\vetau\big)^2
        +\frac{\beta}{M\alpha}p^2
        +2\beta \big(\vetau:(\tsC^u)^{-1}:\tsI\big)p
        \bigg)\dd\Domn
        \\&\hspace{1cm}
        =
        -\int_\Domn (\Psi\veq)\cdot\veq\dd\Domn
        -\int_\Surf \frac1{4b_1}\Big(
        \big(2b_1\jmp{\vev}+b_3\jmp{\ven\cdot\veq}\ven\big)^2+
        (4b_1b_2-b_3^2)\jmp{\ven\cdot\veq}^2\Big)\dd\Surf
    \end{aligned}
    \label{eq:penalty energy}
\end{align}


\end{document}

