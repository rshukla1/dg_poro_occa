syms c11 c12 c13 real;
syms c33 real;
syms c55 real ;
syms a1 a2 a3 real;
syms M real;
syms  m1 m2 m3 real;
syms  rho rhof real;


C=[c11 c12 c13 0 0 0;
   c12 c11 c13 0 0 0;
   c13 c13 c33 0 0 0; 
   0 0 0 c55 0 0;
   0 0 0 0 c55 0;
   0 0 0 0 0 (c11-c12)/2;
];

inC=simplify(inv(C));
alpha=[a1;a2;a3;0;0;0];
S11=inC;
S12=S11*alpha;
S21=alpha'*inC;
S22=(1/M) + alpha'*inC*alpha;
Es=[S11 S12;
    S21 S22];

Ev=[rho 0 0 rhof 0 0;
    0 rho 0 0 rhof 0;
    0 0 rho 0 0 rhof;
    rhof 0 0 m1 0 0;
    0 rhof 0 0 m2 0;
    0 0 rhof 0 0 m3;
    
];

Q=blkdiag(Es,Ev);
inQ=inv(Q)

