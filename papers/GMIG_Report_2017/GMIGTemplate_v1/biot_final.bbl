\begin{thebibliography}{10}

\bibitem{biot1956theory}
{\sc Maurice~A Biot}, {\em Theory of propagation of elastic waves in a
  fluid-saturated porous solid. {I}. {L}ow-frequency range}, The Journal of the
  acoustical Society of america, 28 (1956), pp.~168--178.

\bibitem{biot1962mechanics}
\leavevmode\vrule height 2pt depth -1.6pt width 23pt, {\em Mechanics of
  deformation and acoustic propagation in porous media}, Journal of applied
  physics, 33 (1962), pp.~1482--1498.

\bibitem{caputo1967linear}
{\sc Michele Caputo}, {\em Linear models of dissipation whose {Q} is almost
  frequency independent---{II}}, Geophysical Journal International, 13 (1967),
  pp.~529--539.

\bibitem{carcione1996wave}
{\sc Jos{\'e}~M Carcione}, {\em Wave propagation in anisotropic, saturated
  porous media: Plane-wave theory and numerical simulation}, The Journal of the
  Acoustical Society of America, 99 (1996), pp.~2655--2666.

\bibitem{carcione1998viscoelastic}
\leavevmode\vrule height 2pt depth -1.6pt width 23pt, {\em Viscoelastic
  effective rheologies for modelling wave propagation in porous media},
  Geophysical prospecting, 46 (1998), pp.~249--270.

\bibitem{carcione2007wave}
\leavevmode\vrule height 2pt depth -1.6pt width 23pt, {\em Wave fields in real
  media: Wave propagation in anisotropic, anelastic, porous and electromagnetic
  media}, vol.~38, Elsevier, 2007.

\bibitem{carcione1999numerical}
{\sc Jos{\'e}~M Carcione and Hans~B Helle}, {\em Numerical solution of the
  poroviscoelastic wave equation on a staggered mesh}, Journal of Computational
  Physics, 154 (1999), pp.~520--527.

\bibitem{carcione2002seismic}
{\sc Jose~M Carcione, G{\'e}rard~C Herman, and APE Ten~Kroode}, {\em Seismic
  modeling}, Geophysics, 67 (2002), pp.~1304--1325.

\bibitem{carcione1995some}
{\sc Jos{\'e}~M Carcione and Gerardo Quiroga-Goode}, {\em Some aspects of the
  physics and numerical modeling of {B}iot compressional waves}, Journal of
  Computational Acoustics, 3 (1995), pp.~261--280.

\bibitem{dai1995wave}
{\sc N~Dai, A~Vafidis, and ER~Kanasewich}, {\em Wave propagation in
  heterogeneous, porous media: A velocity-stress, finite-difference method},
  Geophysics, 60 (1995), pp.~327--340.

\bibitem{darcy1856fontaines}
{\sc Henry Darcy}, {\em Les fontaines publique de la ville de {D}ijon},
  Dalmont, Paris, 647 (1856).

\bibitem{de2008discontinuous}
{\sc Josep de~la Puente, Michael Dumbser, Martin K{\"a}ser, and Heiner Igel},
  {\em Discontinuous {G}alerkin methods for wave propagation in poroelastic
  media}, Geophysics, 73 (2008), pp.~T77--T97.

\bibitem{deresiewicz1963uniqueness}
{\sc H~Deresiewicz and R~Skalak}, {\em On uniqueness in dynamic
  poroelasticity}, Bulletin of the Seismological Society of America, 53 (1963),
  pp.~783--788.

\bibitem{desch1988exponential}
{\sc Wolfgang Desch, Richard~K Miller, et~al.}, {\em Exponential stabilization
  of volterra integral equations with singular kernels}, J. Integral Equations
  Appl, 1 (1988), pp.~397--433.

\bibitem{garg1974compressional}
{\sc SK~Garg, Adnan~H Nayfeh, and AJ~Good}, {\em Compressional waves in
  fluid-saturated elastic porous media}, Journal of Applied Physics, 45 (1974),
  pp.~1968--1974.

\bibitem{gurevich1999interface}
{\sc Boris Gurevich and Michael Schoenberg}, {\em Interface conditions for
  {B}iot's equations of poroelasticity}, The Journal of the Acoustical Society
  of America, 105 (1999), pp.~2585--2589.

\bibitem{hassanzadeh1991acoustic}
{\sc Siamak Hassanzadeh}, {\em Acoustic modeling in fluid-saturated porous
  media}, Geophysics, 56 (1991), pp.~424--435.

\bibitem{hesthaven2007nodal}
{\sc Jan~S Hesthaven and Tim Warburton}, {\em Nodal discontinuous {G}alerkin
  methods: algorithms, analysis, and applications}, Springer Science \&
  Business Media, 2007.

\bibitem{jain1979numerical}
{\sc Mahinder~Kumar Jain}, {\em Numerical solution of differential equations},
  (1979).

\bibitem{johnson1987theory}
{\sc David~Linton Johnson, Joel Koplik, and Roger Dashen}, {\em Theory of
  dynamic permeability and tortuosity in fluid-saturated porous media}, Journal
  of fluid mechanics, 176 (1987), pp.~379--402.

\bibitem{lemoine2013high}
{\sc Grady~I Lemoine, M~Yvonne Ou, and Randall~J LeVeque}, {\em High-resolution
  finite volume modeling of wave propagation in orthotropic poroelastic media},
  SIAM Journal on Scientific Computing, 35 (2013), pp.~B176--B206.

\bibitem{mikhailenko1985numerical}
{\sc BG~Mikhailenko}, {\em Numerical experiment in seismic investigations},
  JOURNAL OF GEOPHYSICS-ZEITSCHRIFT FUR GEOPHYSIK, 58 (1985), pp.~101--124.

\bibitem{ozdenvar1997algorithms}
{\sc Turgut {\"O}zdenvar and George~A McMechan}, {\em Algorithms for
  staggered-grid computations for poroelastic, elastic, acoustic, and scalar
  wave equations}, Geophysical Prospecting, 45 (1997), pp.~403--420.

\bibitem{podlubny1998fractional}
{\sc Igor Podlubny}, {\em Fractional differential equations: an introduction to
  fractional derivatives, fractional differential equations, to methods of
  their solution and some of their applications}, vol.~198, Academic press,
  1998.

\bibitem{rosenbaum1974synthetic}
{\sc JH~Rosenbaum}, {\em Synthetic microseismograms: Logging in porous
  formations}, Geophysics, 39 (1974), pp.~14--32.

\bibitem{stern1985wave}
{\sc M~Stern, A~Bedford, and HR~Millwater}, {\em Wave reflection from a
  sediment layer with depth-dependent properties}, The Journal of the
  Acoustical Society of America, 77 (1985), pp.~1781--1788.

\bibitem{strang1968construction}
{\sc Gilbert Strang}, {\em On the construction and comparison of difference
  schemes}, SIAM Journal on Numerical Analysis, 5 (1968), pp.~506--517.

\bibitem{terzaghi1936shearing}
{\sc K~von Terzaghi}, {\em The shearing resistance of saturated soils and the
  angle between the planes of shear}, in Proceedings of the 1st international
  conference on soil mechanics and foundation engineering, vol.~1, Harvard
  University Press Cambridge, MA, 1936, pp.~54--56.

\bibitem{virieux1986p}
{\sc Jean Virieux}, {\em P-sv wave propagation in heterogeneous media:
  Velocity-stress finite-difference method}, Geophysics, 51 (1986),
  pp.~889--901.

\bibitem{warburton2010low}
{\sc T~Warburton}, {\em A low storage curvilinear discontinuous {G}alerkin
  time-domain method for electromagnetics}, in Electromagnetic Theory (EMTS),
  2010 URSI International Symposium on, IEEE, 2010, pp.~996--999.

\bibitem{warburton2013low}
{\sc Timothy Warburton}, {\em A low-storage curvilinear discontinuous
  {G}alerkin method for wave problems}, SIAM Journal on Scientific Computing,
  35 (2013), pp.~A1987--A2012.

\bibitem{yamamoto1988acoustic}
{\sc Tokuo Yamamoto and Altan Turgut}, {\em Acoustic wave propagation through
  porous media with arbitrary pore size distributions}, The Journal of the
  Acoustical Society of America, 83 (1988), pp.~1744--1751.

\bibitem{ye2016discontinuous}
{\sc Ruichao Ye, V~Maarten, Christopher~L Petrovitch, Laura~J Pyrak-Nolte, and
  Lucas~C Wilcox}, {\em A discontinuous {G}alerkin method with a modified
  penalty flux for the propagation and scattering of acousto-elastic waves},
  Geophysical Journal International, 205 (2016), pp.~1267--1289.

\bibitem{zhu1991numerical}
{\sc X~Zhu and GA~McMechan}, {\em Numerical simulation of seismic responses of
  poroelastic reservoirs using {B}iot theory}, Geophysics, 56 (1991),
  pp.~328--339.

\end{thebibliography}
