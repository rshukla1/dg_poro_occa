x=load('x.dat');
y=load('y.dat');
z=load('z.dat');
p1=load('Vz.dat');
p2=load('Qz.dat');
p=p1+ (1.040/1.660)*p2;
p=p1;
x=x';
y=y';
z=z';
p=p';


[pNp,Nele]=size(x);
Nplot=4;

tt=tetsubele(Nplot);
Nsele=size(tt,1);
tt=tt';

scalname={'Vz'};
Nscal=length(scalname);
vectname={};
Nvect=length(vectname);

vis_Nnod=Nele*pNp;
vis_Nele=Nele*Nsele;

outfile='Vz';

fid=vtuxml_head(outfile,vis_Nnod,vis_Nele,scalname,vectname);
fclose(fid);

fid=fopen([outfile,'.vtu'],'a');
vtuxml_xyz(fid,[x(:),y(:),z(:)]);
disp('coordinate done')
%clear x y z;

t=pNp*ones(4*Nsele,1)*(0:Nele-1);
t=t+tt(:)*ones(1,Nele);
t=reshape(t,4,vis_Nele)-1;

vtuxml_conn(fid,t);
disp('connection done')
%clear t;

vtuxml_var(fid,p(:));
disp('scalar done')
%clear p;

vtuxml_end(fid);

